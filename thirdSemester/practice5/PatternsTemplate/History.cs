﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PatternsTemplate
{
    public partial class History : Form, IObserver
    {
        public History()
        {
            InitializeComponent();
        }
        public void UpdatePrice(IModel model, ModelEventArgs e)
        {
            this.Show();
            
            textBoxDestination.Text = e.Flight.Destination;
            textBoxClass.Text = e.Flight.FlightClass.ToString();
            textBoxAdults.Text = e.Flight.Passengers.NumberOfAdults.ToString();
            textBoxChildren.Text = e.Flight.Passengers.NumberOfChildren.ToString();
            textBoxBabies.Text = e.Flight.Passengers.NumberOfBabies.ToString();
            textBoxFlight.Text = e.Flight.Time.ToString();
            textBoxPrice.Text = e.Flight.Price.ToString();
        }

        public void UpdateForm(IModel model, ModelEventArgs e)
        {
            this.Show();

            if (e.Flight != null)
            {
                textBoxDestination.Text = e.Flight.Destination;
                textBoxClass.Text = e.Flight.FlightClass.ToString();
                textBoxAdults.Text = e.Flight.Passengers.NumberOfAdults.ToString();
                textBoxChildren.Text = e.Flight.Passengers.NumberOfChildren.ToString();
                textBoxBabies.Text = e.Flight.Passengers.NumberOfBabies.ToString();
                textBoxFlight.Text = e.Flight.Time.ToString();
                textBoxPrice.Text = e.Flight.Price.ToString();
            }
        }

        private void History_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
        }
    }
}
