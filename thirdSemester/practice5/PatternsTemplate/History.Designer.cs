﻿namespace PatternsTemplate
{
    partial class History
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.labelHistory = new System.Windows.Forms.Label();
            this.tableLayoutPanelFlight = new System.Windows.Forms.TableLayoutPanel();
            this.textBoxPrice = new System.Windows.Forms.TextBox();
            this.textBoxFlight = new System.Windows.Forms.TextBox();
            this.textBoxClass = new System.Windows.Forms.TextBox();
            this.textBoxBabies = new System.Windows.Forms.TextBox();
            this.textBoxChildren = new System.Windows.Forms.TextBox();
            this.textBoxAdults = new System.Windows.Forms.TextBox();
            this.labelDestination = new System.Windows.Forms.Label();
            this.labelAdults = new System.Windows.Forms.Label();
            this.labelChildren = new System.Windows.Forms.Label();
            this.labelBabies = new System.Windows.Forms.Label();
            this.labelClass = new System.Windows.Forms.Label();
            this.labelFlight = new System.Windows.Forms.Label();
            this.labelPrice = new System.Windows.Forms.Label();
            this.textBoxDestination = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel.SuspendLayout();
            this.tableLayoutPanelFlight.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 1;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 345F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Controls.Add(this.labelHistory, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.tableLayoutPanelFlight, 0, 1);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 2;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 145F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(345, 405);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // labelHistory
            // 
            this.labelHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelHistory.AutoSize = true;
            this.labelHistory.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelHistory.Location = new System.Drawing.Point(3, 8);
            this.labelHistory.Name = "labelHistory";
            this.labelHistory.Size = new System.Drawing.Size(339, 25);
            this.labelHistory.TabIndex = 3;
            this.labelHistory.Text = "History";
            this.labelHistory.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanelFlight
            // 
            this.tableLayoutPanelFlight.ColumnCount = 2;
            this.tableLayoutPanelFlight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelFlight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelFlight.Controls.Add(this.textBoxPrice, 1, 6);
            this.tableLayoutPanelFlight.Controls.Add(this.textBoxFlight, 1, 5);
            this.tableLayoutPanelFlight.Controls.Add(this.textBoxClass, 1, 4);
            this.tableLayoutPanelFlight.Controls.Add(this.textBoxBabies, 1, 3);
            this.tableLayoutPanelFlight.Controls.Add(this.textBoxChildren, 1, 2);
            this.tableLayoutPanelFlight.Controls.Add(this.textBoxAdults, 1, 1);
            this.tableLayoutPanelFlight.Controls.Add(this.labelDestination, 0, 0);
            this.tableLayoutPanelFlight.Controls.Add(this.labelAdults, 0, 1);
            this.tableLayoutPanelFlight.Controls.Add(this.labelChildren, 0, 2);
            this.tableLayoutPanelFlight.Controls.Add(this.labelBabies, 0, 3);
            this.tableLayoutPanelFlight.Controls.Add(this.labelClass, 0, 4);
            this.tableLayoutPanelFlight.Controls.Add(this.labelFlight, 0, 5);
            this.tableLayoutPanelFlight.Controls.Add(this.labelPrice, 0, 6);
            this.tableLayoutPanelFlight.Controls.Add(this.textBoxDestination, 1, 0);
            this.tableLayoutPanelFlight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelFlight.Location = new System.Drawing.Point(3, 45);
            this.tableLayoutPanelFlight.Name = "tableLayoutPanelFlight";
            this.tableLayoutPanelFlight.RowCount = 7;
            this.tableLayoutPanelFlight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanelFlight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanelFlight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanelFlight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanelFlight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanelFlight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanelFlight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanelFlight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelFlight.Size = new System.Drawing.Size(339, 357);
            this.tableLayoutPanelFlight.TabIndex = 1;
            // 
            // textBoxPrice
            // 
            this.textBoxPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBoxPrice.Location = new System.Drawing.Point(172, 315);
            this.textBoxPrice.Name = "textBoxPrice";
            this.textBoxPrice.ReadOnly = true;
            this.textBoxPrice.Size = new System.Drawing.Size(164, 26);
            this.textBoxPrice.TabIndex = 14;
            // 
            // textBoxFlight
            // 
            this.textBoxFlight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFlight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBoxFlight.Location = new System.Drawing.Point(172, 262);
            this.textBoxFlight.Name = "textBoxFlight";
            this.textBoxFlight.ReadOnly = true;
            this.textBoxFlight.Size = new System.Drawing.Size(164, 26);
            this.textBoxFlight.TabIndex = 13;
            // 
            // textBoxClass
            // 
            this.textBoxClass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBoxClass.Location = new System.Drawing.Point(172, 212);
            this.textBoxClass.Name = "textBoxClass";
            this.textBoxClass.ReadOnly = true;
            this.textBoxClass.Size = new System.Drawing.Size(164, 26);
            this.textBoxClass.TabIndex = 12;
            // 
            // textBoxBabies
            // 
            this.textBoxBabies.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxBabies.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBoxBabies.Location = new System.Drawing.Point(172, 162);
            this.textBoxBabies.Name = "textBoxBabies";
            this.textBoxBabies.ReadOnly = true;
            this.textBoxBabies.Size = new System.Drawing.Size(164, 26);
            this.textBoxBabies.TabIndex = 11;
            // 
            // textBoxChildren
            // 
            this.textBoxChildren.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxChildren.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBoxChildren.Location = new System.Drawing.Point(172, 112);
            this.textBoxChildren.Name = "textBoxChildren";
            this.textBoxChildren.ReadOnly = true;
            this.textBoxChildren.Size = new System.Drawing.Size(164, 26);
            this.textBoxChildren.TabIndex = 10;
            // 
            // textBoxAdults
            // 
            this.textBoxAdults.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxAdults.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBoxAdults.Location = new System.Drawing.Point(172, 62);
            this.textBoxAdults.Name = "textBoxAdults";
            this.textBoxAdults.ReadOnly = true;
            this.textBoxAdults.Size = new System.Drawing.Size(164, 26);
            this.textBoxAdults.TabIndex = 9;
            // 
            // labelDestination
            // 
            this.labelDestination.AutoSize = true;
            this.labelDestination.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDestination.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDestination.Location = new System.Drawing.Point(3, 0);
            this.labelDestination.Name = "labelDestination";
            this.labelDestination.Size = new System.Drawing.Size(163, 50);
            this.labelDestination.TabIndex = 1;
            this.labelDestination.Text = "Destination";
            this.labelDestination.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelAdults
            // 
            this.labelAdults.AutoSize = true;
            this.labelAdults.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelAdults.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelAdults.Location = new System.Drawing.Point(3, 50);
            this.labelAdults.Name = "labelAdults";
            this.labelAdults.Size = new System.Drawing.Size(163, 50);
            this.labelAdults.TabIndex = 2;
            this.labelAdults.Text = "Adults";
            this.labelAdults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelChildren
            // 
            this.labelChildren.AutoSize = true;
            this.labelChildren.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelChildren.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelChildren.Location = new System.Drawing.Point(3, 100);
            this.labelChildren.Name = "labelChildren";
            this.labelChildren.Size = new System.Drawing.Size(163, 50);
            this.labelChildren.TabIndex = 3;
            this.labelChildren.Text = "Children";
            this.labelChildren.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelBabies
            // 
            this.labelBabies.AutoSize = true;
            this.labelBabies.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelBabies.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelBabies.Location = new System.Drawing.Point(3, 150);
            this.labelBabies.Name = "labelBabies";
            this.labelBabies.Size = new System.Drawing.Size(163, 50);
            this.labelBabies.TabIndex = 4;
            this.labelBabies.Text = "Babies";
            this.labelBabies.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelClass
            // 
            this.labelClass.AutoSize = true;
            this.labelClass.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelClass.Location = new System.Drawing.Point(3, 200);
            this.labelClass.Name = "labelClass";
            this.labelClass.Size = new System.Drawing.Size(163, 50);
            this.labelClass.TabIndex = 5;
            this.labelClass.Text = "Class";
            this.labelClass.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelFlight
            // 
            this.labelFlight.AutoSize = true;
            this.labelFlight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelFlight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelFlight.Location = new System.Drawing.Point(3, 250);
            this.labelFlight.Name = "labelFlight";
            this.labelFlight.Size = new System.Drawing.Size(163, 50);
            this.labelFlight.TabIndex = 6;
            this.labelFlight.Text = "Flight";
            this.labelFlight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPrice
            // 
            this.labelPrice.AutoSize = true;
            this.labelPrice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelPrice.Location = new System.Drawing.Point(3, 300);
            this.labelPrice.Name = "labelPrice";
            this.labelPrice.Size = new System.Drawing.Size(163, 57);
            this.labelPrice.TabIndex = 7;
            this.labelPrice.Text = "Price";
            this.labelPrice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxDestination
            // 
            this.textBoxDestination.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxDestination.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBoxDestination.Location = new System.Drawing.Point(172, 12);
            this.textBoxDestination.Name = "textBoxDestination";
            this.textBoxDestination.ReadOnly = true;
            this.textBoxDestination.Size = new System.Drawing.Size(164, 26);
            this.textBoxDestination.TabIndex = 8;
            // 
            // History
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(345, 405);
            this.Controls.Add(this.tableLayoutPanel);
            this.MinimumSize = new System.Drawing.Size(300, 30);
            this.Name = "History";
            this.Text = "History";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.History_FormClosing);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.tableLayoutPanelFlight.ResumeLayout(false);
            this.tableLayoutPanelFlight.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelFlight;
        private System.Windows.Forms.Label labelDestination;
        private System.Windows.Forms.Label labelAdults;
        private System.Windows.Forms.Label labelChildren;
        private System.Windows.Forms.Label labelBabies;
        private System.Windows.Forms.Label labelClass;
        private System.Windows.Forms.Label labelFlight;
        private System.Windows.Forms.Label labelPrice;
        private System.Windows.Forms.TextBox textBoxDestination;
        private System.Windows.Forms.TextBox textBoxPrice;
        private System.Windows.Forms.TextBox textBoxFlight;
        private System.Windows.Forms.TextBox textBoxClass;
        private System.Windows.Forms.TextBox textBoxBabies;
        private System.Windows.Forms.TextBox textBoxChildren;
        private System.Windows.Forms.TextBox textBoxAdults;
        private System.Windows.Forms.Label labelHistory;
    }
}