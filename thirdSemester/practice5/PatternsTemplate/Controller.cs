﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsTemplate
{
    public class Controller : IController
    {
        IView view;
        IModel model;

        public Controller(IView view, IModel model)
        {
            this.view = view;
            this.model = model;

            this.view.SetController(this);
            if (view is IObserver)
                this.model.Attach((IObserver)view);
        }

        public void CalculatePrice()
        {
            if (view.IsValid())
            {
                Flight flight = view.GetFlight();
                flight.Price = CalculationFacade.GetPrice(flight);
                model.Push(flight);
            }
        }
        public void addNewObserver(IObserver observer)
        {
            this.model.Attach(observer);
        }
        public void Undo()
        {
            model.Pop();
        }
    }
}
