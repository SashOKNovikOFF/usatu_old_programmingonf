﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication_Sixth
{
    public partial class Description : Form
    {
        public Description()
        {
            InitializeComponent();

            string tempDescription = "\"Змейка\": в программе есть направление движения, меняемое кнопками. ";
            tempDescription = string.Concat(tempDescription, "На каждом шаге в очередь добавляется одна ячейка в этом направлении от текущей, с другого конца одна убирается. ");
            tempDescription = string.Concat(tempDescription, "Очередь на связанном списке.");

            textBoxDescription.Text = tempDescription;

            string tempInstruction = "Для запуска игры необходимо выполнить следующие действия:\r\n";
            tempInstruction = string.Concat(tempInstruction, "1. Запустить окно во вкладке \"Работа\" -> \"Задача\".\r\n");
            tempInstruction = string.Concat(tempInstruction, "2. Внести необходимые данные (\"Добавить стену\", \"Добавить еду\"). ");
            tempInstruction = string.Concat(tempInstruction, "Стену или еду можно добавлять на самом поле, которое отображается в окне \"Иллюстрация\". ");
            tempInstruction = string.Concat(tempInstruction, "Но перед этим необходимо нажать на кнопку \"Добавлять на поле стену\\еду\" для выбора нужного элемента.\r\n");
            tempInstruction = string.Concat(tempInstruction, "3. Нажать на кнопку \"Запуск\". Если змейка остановилась, вы можете поменять направление при помощи стрелок в окне \"Задача\". ");
            tempInstruction = string.Concat(tempInstruction, "В окне \"Задача\" кнопка \"Стоп\" останавливает змейку, \"Рестарт\" - перезапускает игру, \"Следующий шаг\" - пошагово перемещает змейку.\r\n");
            tempInstruction = string.Concat(tempInstruction, "4. Для отображения координат змеи, необходимо запустить окно \"Структуры данных\".\r\n");
            tempInstruction = string.Concat(tempInstruction, "5. Кнопка \"Создать\" - перезапускает игру с возможностью сохранения данных по игре, \"Сохранить в файл\" - сохраняет данные по игре.");

            textBoxInstruction.Text = tempInstruction;
        }
    }
}
