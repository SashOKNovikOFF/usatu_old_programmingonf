﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

using System.IO;

namespace WindowsFormsApplication_Sixth
{
    public sealed class Data
    {
        // данные по полю
        public int CounterWall
        {
            set;
            get;
        }
        public int CounterFood
        {
            set;
            get;
        }
        public int MaxWall
        {
            set;
            get;
        }
        public int MaxFood
        {
            set;
            get;
        }
        public int RowCount
        {
            set;
            get;
        }
        public int ColumnCount
        {
            set;
            get;
        }
        const int cellsMaxWidth = 100;
        const int cellsMaxHeight = 100;
        int[,] walls = new int[cellsMaxWidth, cellsMaxHeight];
        int[,] foods = new int[cellsMaxWidth, cellsMaxHeight];
        public void SetWall(int i, int j, int value)
        {
            walls[i, j] = value;
        }
        public int GetWall(int i, int j)
        {
            return walls[i, j];
        }
        public void SetFood(int i, int j, int value)
        {
            foods[i, j] = value;
        }
        public int GetFood(int i, int j)
        {
            return foods[i, j];
        }

        // данные по классу Data
        private static readonly Data instance = new Data();
        public static Data Instance
        {
            get { return instance; }
        }
        private Data()
        {
            // добавляем змейку на экран
            Snake = new UserQueue<Point>();
            Snake.Enqueue(new Point(0, 0));
            Snake.Enqueue(new Point(1, 0));
            Snake.Enqueue(new Point(2, 0));

            SnakeHeader = new Point(2, 0);

            CounterWall = 0;
            CounterFood = 0;
            MaxWall = 15;
            MaxFood = 15;

            ColumnCount = 20;
            RowCount = 12;

            CurrentStepNumber = 0;
            IsDataOld = false;

            SetMode = GridSetMode.NaN;
            SetMove = SnakeMove.Right;
        }
        
        // данные по змейке
        public UserQueue<Point> Snake
        {
            set;
            get;
        }
        public Point SnakeHeader
        {
            set;
            get;
        }
        public bool isSnakeHere(int column, int row)
        {
            if (Snake == null)
                return false;
            List<Point> listSnake = Snake.ToList<Point>();
            Point tempPoint = new Point(column, row);

            return listSnake.Contains(tempPoint);
        }
        public bool RunSnake()
        {
            // позволяем главной форме "пересоздать" игру
            IsDataOld = true;

            // переменная-флаг, отвечающая за удаление хвоста
            bool tailDelete = true;

            // временная переменная для редактирования будущих координат головы змеи
            Point tempPointHeader = new Point(SnakeHeader.X, SnakeHeader.Y);

            // меняем направление движения змеи
            if (SetMove == SnakeMove.Right)
                tempPointHeader.X++;
            else if (SetMove == SnakeMove.Left)
                tempPointHeader.X--;
            else if (SetMove == SnakeMove.Up)
                tempPointHeader.Y--;
            else if (SetMove == SnakeMove.Down)
                tempPointHeader.Y++;

            // если змейка уходит за пределы экрана
            if ((tempPointHeader.X < 0) || (tempPointHeader.X >= ColumnCount) || (tempPointHeader.Y < 0) || (tempPointHeader.Y >= RowCount))
            {
                DataChanged(this, new DataEventArgs("Змейка остановилась (впереди ограждение)"));
                return false;
            }
            
            // проверка на наличие препятствий
            if ((walls[tempPointHeader.X, tempPointHeader.Y] == 1) || isSnakeHere(tempPointHeader.X, tempPointHeader.Y))
            {
                DataChanged(this, new DataEventArgs("Змейка остановилась (впереди стена или тело змеи)"));
                return false;
            }
            else if (foods[tempPointHeader.X, tempPointHeader.Y] == 1)
            {
                foods[tempPointHeader.X, tempPointHeader.Y] = 0;
                CounterFood--;
                tailDelete = false;
            }

            // перемещаем змею
            SnakeHeader = tempPointHeader;
            Snake.Enqueue(SnakeHeader);
            if (tailDelete)
                Snake.Dequeue();

            CurrentStepNumber++;
            DataChanged(this, new DataEventArgs("Змейка начала движение"));
            return true;
        }

        // событие по изменению и очищению данных в классе Data
        public event EventHandler<DataEventArgs> DataChanged;
        public event EventHandler<DataEventArgs> DataCleared;

        // функции, вносящие изменения в данные
        public void AddWall(int column, int row)
        {
            Point tempPoint = new Point(column, row);

            // проверка на наличие в клетке змеи
            if (isSnakeHere(tempPoint.X, tempPoint.Y))
            {
                if (DataChanged != null)
                    DataChanged(this, new DataEventArgs("Нельзя добавить стену сюда!"));
                return;
            }

            // позволяем главной форме "пересоздать" игру
            IsDataOld = true;

            // проверка на переполнение стен
            if (CounterWall == MaxWall)
            {
                if (DataChanged != null)
                    DataChanged(this, new DataEventArgs("Больше нельзя добавить стену на поле!"));
                return;
            }
            // проверка на наличие в клетке еды
            if (foods[tempPoint.X, tempPoint.Y] == 1)
            {
                foods[tempPoint.X, tempPoint.Y] = 0;
                CounterFood--;
            }

            // добавить стену, увеличить счётчик
            if (walls[tempPoint.X, tempPoint.Y] == 0)
                CounterWall++;
            walls[tempPoint.X, tempPoint.Y] = 1;

            // обновить информацию
            DataChanged(this, new DataEventArgs("Координаты: (" + (row + 1) + ", " + (column + 1) + ")"));
        }
        public void AddFood(int column, int row)
        {
            Point tempPoint = new Point(column, row);

            // проверка на наличие в клетке змеи
            if (isSnakeHere(tempPoint.X, tempPoint.Y))
            {
                if (DataChanged != null)
                    DataChanged(this, new DataEventArgs("Нельзя добавить еду сюда!"));
                return;
            }

            // позволяем главной форме "пересоздать" игру
            IsDataOld = true;

            // проверка на переполнение еды
            if (CounterFood == MaxFood)
            {
                if (DataChanged != null)
                    DataChanged(this, new DataEventArgs("Больше нельзя добавить еду на поле!"));
                return;
            }
            // проверка на наличие в клетке стены
            if (walls[tempPoint.X, tempPoint.Y] == 1)
            {
                walls[tempPoint.X, tempPoint.Y] = 0;
                CounterWall--;
            }

            // добавить еду, увеличить счётчик
            if (foods[tempPoint.X, tempPoint.Y] == 0)
                CounterFood++;
            foods[tempPoint.X, tempPoint.Y] = 1;

            // обновить информацию
            DataChanged(this, new DataEventArgs("Координаты: (" + (row + 1) + ", " + (column + 1) + ")"));
        }
        public void AddWallFood(int column, int row)
        {
            // добавляем либо стену, либо еду, либо ничего
            if (Data.Instance.SetMode == Data.GridSetMode.NaN)
                DataChanged(this, new DataEventArgs("Не выбран элемент для добавления (раздел \"Работа\", пункт \"Задача\", кнопка \"Добавлять на поле стену\\еду\")."));
            else
            // если добавляем еду
            if (Data.Instance.SetMode == Data.GridSetMode.Eat)
                AddFood(column, row);
            // если добавляем стену
            else if (SetMode == GridSetMode.Wall)
                AddWall(column, row);
        }
        public void ChangeWallFood()
        {
            if (SetMode == GridSetMode.Wall)
                SetMode = GridSetMode.Eat;
            else
                SetMode = GridSetMode.Wall;
        }
        public void ChangeDirection(SnakeMove direction)
        {
            if (direction == SnakeMove.Up)
                if (!(SetMove == SnakeMove.Down))
                    SetMove = SnakeMove.Up;
            if (direction == SnakeMove.Down)
                if (!(SetMove == SnakeMove.Up))
                    SetMove = SnakeMove.Down;
            if (direction == SnakeMove.Left)
                if (!(SetMove == SnakeMove.Right))
                    SetMove = SnakeMove.Left;
            if (direction == SnakeMove.Right)
                if (!(SetMove == SnakeMove.Left))
                    SetMove = SnakeMove.Right;
        }
        public void SaveData()
        {
            using (StreamWriter sw = new StreamWriter("Data.txt", false))
            {
                // переменная-флаг для вывода данных о стенах/еде
                bool flag = false;

                sw.WriteLine("Добавленные стены:");
                for (int i = 0; i < ColumnCount; i++)
                    for (int j = 0; j < RowCount; j++)
                        if (walls[i, j] == 1)
                        {
                            sw.WriteLine("Координаты: (" + (i + 1) + ", " + (j + 1) + ")\n");
                            flag = true;
                        }
                if (!flag)
                    sw.WriteLine("стены на поле отсутствуют.");
                sw.WriteLine();

                sw.WriteLine("Добавленная еда:");
                for (int i = 0; i < ColumnCount; i++)
                    for (int j = 0; j < RowCount; j++)
                        if (foods[i, j] == 1)
                        {
                            sw.WriteLine("Координаты: (" + (i + 1) + ", " + (j + 1) + ")\n");
                            flag = true;
                        }
                if (!flag)
                    sw.WriteLine("еда на поле отсутствует.");
                sw.WriteLine();

                sw.WriteLine("Координаты тела змеи:");
                List<Point> listSnake = new List<Point>();
                listSnake = Snake.ToList<Point>();
                for (int i = 0; i < listSnake.Count; i++)
                    sw.WriteLine("Координаты: (" + (listSnake[i].X + 1) + ", " + (listSnake[i].Y + 1) + ")");
            }
        }
        public void ClearData()
        {
            // очищаем сетку
            for (int i = 0; i < ColumnCount; i++)
                for (int j = 0; j < RowCount; j++)
                {
                    walls[i, j] = 0;
                    foods[i, j] = 0;
                }

            // удаляем старую змейку
            Snake.Clear();
            // добавляем змейку на экран
            Snake.Enqueue(new Point(0, 0));
            Snake.Enqueue(new Point(1, 0));
            Snake.Enqueue(new Point(2, 0));
            SnakeHeader = new Point(2, 0);
            
            // очищаем счётчики
            CounterWall = 0;
            CounterFood = 0;

            // очищаем шаг
            CurrentStepNumber = 0;

            // запрещаем главной форме "пересоздать" игру
            IsDataOld = false;

            // сбрасываем все данные по передвижению и перемещению
            SetMode = GridSetMode.NaN;
            SetMove = SnakeMove.Right;

            // очищаем формы
            DataCleared(this, new DataEventArgs("Все данные успешно очищены"));
        }

        // разное
        public bool IsDataOld
        {
            set;
            get;
        }
        public int CurrentStepNumber
        {
            get;
            set;
        }
        public enum GridSetMode { Wall, Eat, NaN };
        public GridSetMode SetMode
        {
            set;
            get;
        }
        public enum SnakeMove { Up, Down, Left, Right };
        public SnakeMove SetMove
        {
            set;
            get;
        }
    }

    public class DataEventArgs : EventArgs
    {
        public readonly string message;
        public DataEventArgs(string message)
        {
            this.message = message;
        }
    }
}
