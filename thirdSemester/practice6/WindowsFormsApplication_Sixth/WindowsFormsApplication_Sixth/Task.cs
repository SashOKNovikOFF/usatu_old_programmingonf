﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication_Sixth
{
    public partial class Task : Form
    {
        public Task()
        {
            InitializeComponent();

            // ограничиваем ввод координат в форме
            numericWallX.Maximum = Data.Instance.ColumnCount;
            numericFoodX.Maximum = Data.Instance.ColumnCount;
            numericWallY.Maximum = Data.Instance.RowCount;
            numericFoodY.Maximum = Data.Instance.RowCount;

            // регистрируем обработчик событий обновления окна формы при изменении данных
            Data.Instance.DataChanged += UpdateInformation;
            Data.Instance.DataCleared += ClearInformation;
        }

        // при закрытии не уничтожает окно
        private void Task_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }
        
        // обновить информацию на панели
        private void UpdateInformation(object sender, DataEventArgs e)
        {
            string textWall, textFood; // временные переменные для вывода данных о стенах/еде в панель

            // переменная-флаг для вывода данных о стенах/еде
            bool flagWall = false;
            bool flagFood = false;

            textWall = "Добавленные стены:\r\n";
            textFood = "Текущий шаг: " + Data.Instance.CurrentStepNumber + "\r\n";
            textFood = string.Concat(textFood, "Добавленная еда:\r\n");
            for (int i = 0; i < Data.Instance.ColumnCount; i++)
                for (int j = 0; j < Data.Instance.RowCount; j++)
                {
                    if (Data.Instance.GetWall(i, j) == 1)
                    {
                        textWall = string.Concat(textWall, "Координаты: (" + (i + 1) + ", " + (j + 1) + ")\r\n");
                        flagWall = true;
                    }
                    if (Data.Instance.GetFood(i, j) == 1)
                    {
                        textFood = string.Concat(textFood, "Координаты: (" + (i + 1) + ", " + (j + 1) + ")\r\n");
                        flagFood = true;
                    }
                }
            if (!flagWall)
                textWall = string.Concat(textWall, "стены на поле отсутствуют.");
            if (!flagFood)
                textFood = string.Concat(textFood, "еда на поле отсутствует.");

            textBoxListWall.Text = textWall;
            textBoxListFood.Text = textFood;
        }
        // очистить информацию на панели
        private void ClearInformation(object sender, DataEventArgs e)
        {
            // останавливаем таймер
            timer.Stop();

            textBoxListWall.Text = "";
            textBoxListFood.Text = "";
            numericFoodX.Value = 1;
            numericFoodY.Value = 1;
            numericWallX.Value = 1;
            numericWallY.Value = 1;
        }

        // "Добавить стену: "
        private void buttonAddWall_Click(object sender, EventArgs e)
        {
            Data.Instance.AddWall((int)numericWallX.Value - 1, (int)numericWallY.Value - 1);
        }
        // "Добавить еду: "
        private void buttonAddFood_Click(object sender, EventArgs e)
        {
            Data.Instance.AddFood((int)numericFoodX.Value - 1, (int)numericFoodY.Value - 1);
        }
        // менять добавляемый элемент (стену/еду) на поле при помощи мыши
        private void toolButtonAddWallFood_Click(object sender, EventArgs e)
        {
            Data.Instance.ChangeWallFood();
        }

        // работа с таймером
        private void timer_Tick(object sender, EventArgs e)
        {
            bool flag = true;
            flag = Data.Instance.RunSnake();
            if (!flag)
                timer.Stop();
        }
        // запуск таймера
        private void toolButtonRun_Click(object sender, EventArgs e)
        {
            timer.Start();
        }
        // остановка таймера
        private void toolButtonStop_Click(object sender, EventArgs e)
        {
            timer.Stop();
        }

        // изменение направления движения
        private void buttonUp_Click(object sender, EventArgs e)
        {
            Data.Instance.ChangeDirection(Data.SnakeMove.Up);
        }
        private void buttonDown_Click(object sender, EventArgs e)
        {
            Data.Instance.ChangeDirection(Data.SnakeMove.Down);
        }
        private void buttonLeft_Click(object sender, EventArgs e)
        {
            Data.Instance.ChangeDirection(Data.SnakeMove.Left);
        }
        private void buttonRight_Click(object sender, EventArgs e)
        {
            Data.Instance.ChangeDirection(Data.SnakeMove.Right);
        }

        // следующий шаг
        private void toolButtonNextStep_Click(object sender, EventArgs e)
        {
            bool flag = true;
            flag = Data.Instance.RunSnake();
            if (!flag)
                timer.Stop();
        }
        // перезапустить "Змейку"
        private void toolButtonRestart_Click(object sender, EventArgs e)
        {
            timer.Stop();
            Data.Instance.ClearData();
        }
    }
}
