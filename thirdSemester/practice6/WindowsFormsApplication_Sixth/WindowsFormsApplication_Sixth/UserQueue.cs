﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApplication_Sixth
{
    public class Node<T>
    {
        public T Data
        {
            set;
            get;
        }
        public Node<T> Next
        {
            set;
            get;
        }

        public Node(T data)
        {
            this.Data = data;
        }
    }

    public class UserQueue<T>
    {
        Node<T> first, last;

        public void Enqueue(T item)
        {
            Node<T> temp = new Node<T>(item);
            if (last == null)
                first = last = temp;
            else
            {
                last.Next = temp;
                last = temp;
            }
        }
        public T Dequeue()
        {
            Node<T> temp = first;
            if (first == last)
                first = last = null;
            else
            {
                first = first.Next;
                temp.Next = null;
            }

            return temp.Data;
        }
        public void Clear()
        {
            while (first != last)
                Dequeue();
            Dequeue();
        }

        public List<T> ToList<K>()
        {
            List<T> tempList = new List<T>();
            Node<T> tempNode = first;

            bool flag = true;

            if (tempNode == null)
                return null;
            while (flag)
            {
                tempList.Add(tempNode.Data);
                tempNode = tempNode.Next;
                if (tempNode == null)
                    flag = false;
            }

            return tempList;
        }
    }
}
