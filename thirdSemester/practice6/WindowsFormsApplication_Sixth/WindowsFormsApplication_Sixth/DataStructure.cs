﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication_Sixth
{
    public partial class DataStructure : Form
    {
        public DataStructure()
        {
            InitializeComponent();

            // обновить информацию в таблице в первый раз
            List<Point> listSnake = new List<Point>();
            listSnake = Data.Instance.Snake.ToList<Point>();

            dataGridView.Rows.Clear();
            for (int i = 0; i < listSnake.Count; i++)
                dataGridView.Rows.Add(i + 1, (listSnake[i].X + 1), (listSnake[i].Y + 1));

            // регистрируем обработчик событий обновления окна формы при изменении или очищении данных
            Data.Instance.DataChanged += UpdateInformation;
            Data.Instance.DataCleared += UpdateInformation;
        }
        
        // при закрытии не уничтожает окно
        private void DataStructure_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }
        
        // обновить информацию
        private void UpdateInformation(object sender, DataEventArgs e)
        {
            List<Point> listSnake = new List<Point>();
            listSnake = Data.Instance.Snake.ToList<Point>();

            dataGridView.Rows.Clear();
            for (int i = 0; i < listSnake.Count; i++)
                dataGridView.Rows.Add(i + 1, (listSnake[i].X + 1), (listSnake[i].Y + 1));
        }
    }
}
