﻿namespace WindowsFormsApplication_Sixth
{
    partial class Description
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.labelHeaderDescription = new System.Windows.Forms.Label();
            this.labelHeaderInstruction = new System.Windows.Forms.Label();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.textBoxInstruction = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 1;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Controls.Add(this.textBoxInstruction, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.labelHeaderDescription, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.labelHeaderInstruction, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.textBoxDescription, 0, 1);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 4;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(294, 268);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // labelHeaderDescription
            // 
            this.labelHeaderDescription.AutoSize = true;
            this.labelHeaderDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelHeaderDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelHeaderDescription.Location = new System.Drawing.Point(3, 0);
            this.labelHeaderDescription.Name = "labelHeaderDescription";
            this.labelHeaderDescription.Size = new System.Drawing.Size(288, 26);
            this.labelHeaderDescription.TabIndex = 0;
            this.labelHeaderDescription.Text = "Задача программы (вариант 26)";
            this.labelHeaderDescription.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelHeaderInstruction
            // 
            this.labelHeaderInstruction.AutoSize = true;
            this.labelHeaderInstruction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelHeaderInstruction.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelHeaderInstruction.Location = new System.Drawing.Point(3, 106);
            this.labelHeaderInstruction.Name = "labelHeaderInstruction";
            this.labelHeaderInstruction.Size = new System.Drawing.Size(288, 26);
            this.labelHeaderInstruction.TabIndex = 1;
            this.labelHeaderInstruction.Text = "Инструкция по работе с программой";
            this.labelHeaderInstruction.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxDescription.Location = new System.Drawing.Point(3, 29);
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.ReadOnly = true;
            this.textBoxDescription.Size = new System.Drawing.Size(288, 74);
            this.textBoxDescription.TabIndex = 2;
            this.textBoxDescription.TabStop = false;
            this.textBoxDescription.Text = "Здесь будет размещаться условие задачи.";
            // 
            // textBoxInstruction
            // 
            this.textBoxInstruction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxInstruction.Location = new System.Drawing.Point(3, 135);
            this.textBoxInstruction.Multiline = true;
            this.textBoxInstruction.Name = "textBoxInstruction";
            this.textBoxInstruction.ReadOnly = true;
            this.textBoxInstruction.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxInstruction.Size = new System.Drawing.Size(288, 130);
            this.textBoxInstruction.TabIndex = 3;
            this.textBoxInstruction.TabStop = false;
            this.textBoxInstruction.Text = "Здесь будет размещаться инструкция по работе с программой.";
            // 
            // Description
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(294, 268);
            this.Controls.Add(this.tableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Description";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Описание программы";
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Label labelHeaderDescription;
        private System.Windows.Forms.Label labelHeaderInstruction;
        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.TextBox textBoxInstruction;
    }
}