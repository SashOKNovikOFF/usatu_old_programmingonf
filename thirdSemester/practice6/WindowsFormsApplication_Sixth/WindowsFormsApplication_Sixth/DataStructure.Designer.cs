﻿namespace WindowsFormsApplication_Sixth
{
    partial class DataStructure
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tablePanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.labelNameTable = new System.Windows.Forms.Label();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.columnNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.сolumnSnakeX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnSnakeY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tablePanelMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // tablePanelMain
            // 
            this.tablePanelMain.ColumnCount = 1;
            this.tablePanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tablePanelMain.Controls.Add(this.labelNameTable, 0, 0);
            this.tablePanelMain.Controls.Add(this.dataGridView, 0, 1);
            this.tablePanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanelMain.Location = new System.Drawing.Point(0, 0);
            this.tablePanelMain.Name = "tablePanelMain";
            this.tablePanelMain.RowCount = 2;
            this.tablePanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tablePanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tablePanelMain.Size = new System.Drawing.Size(349, 452);
            this.tablePanelMain.TabIndex = 0;
            // 
            // labelNameTable
            // 
            this.labelNameTable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.labelNameTable.AutoSize = true;
            this.labelNameTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelNameTable.Location = new System.Drawing.Point(117, 0);
            this.labelNameTable.Name = "labelNameTable";
            this.labelNameTable.Size = new System.Drawing.Size(114, 45);
            this.labelNameTable.TabIndex = 0;
            this.labelNameTable.Text = "Структура данных";
            this.labelNameTable.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dataGridView
            // 
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnNumber,
            this.сolumnSnakeX,
            this.columnSnakeY});
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(3, 48);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.Size = new System.Drawing.Size(343, 401);
            this.dataGridView.TabIndex = 1;
            // 
            // columnNumber
            // 
            this.columnNumber.HeaderText = "№";
            this.columnNumber.Name = "columnNumber";
            // 
            // сolumnSnakeX
            // 
            this.сolumnSnakeX.HeaderText = "X";
            this.сolumnSnakeX.Name = "сolumnSnakeX";
            // 
            // columnSnakeY
            // 
            this.columnSnakeY.HeaderText = "Y";
            this.columnSnakeY.Name = "columnSnakeY";
            // 
            // DataStructure
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(349, 452);
            this.Controls.Add(this.tablePanelMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "DataStructure";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Структура данных";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DataStructure_FormClosing);
            this.tablePanelMain.ResumeLayout(false);
            this.tablePanelMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tablePanelMain;
        private System.Windows.Forms.Label labelNameTable;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn сolumnSnakeX;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnSnakeY;
    }
}