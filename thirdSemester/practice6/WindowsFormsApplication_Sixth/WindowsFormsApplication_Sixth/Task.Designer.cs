﻿namespace WindowsFormsApplication_Sixth
{
    partial class Task
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Task));
            this.barTool = new System.Windows.Forms.ToolStrip();
            this.toolButtonRun = new System.Windows.Forms.ToolStripButton();
            this.toolButtonStop = new System.Windows.Forms.ToolStripButton();
            this.toolButtonRestart = new System.Windows.Forms.ToolStripButton();
            this.toolButtonNextStep = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolButtonAddWallFood = new System.Windows.Forms.ToolStripButton();
            this.tablePanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutFood = new System.Windows.Forms.TableLayoutPanel();
            this.labelAddFood = new System.Windows.Forms.Label();
            this.labelFoodX = new System.Windows.Forms.Label();
            this.labelFoodY = new System.Windows.Forms.Label();
            this.numericFoodY = new System.Windows.Forms.NumericUpDown();
            this.numericFoodX = new System.Windows.Forms.NumericUpDown();
            this.buttonAddFood = new System.Windows.Forms.Button();
            this.tablePanelWall = new System.Windows.Forms.TableLayoutPanel();
            this.labelAddWall = new System.Windows.Forms.Label();
            this.labelWallX = new System.Windows.Forms.Label();
            this.labelWallY = new System.Windows.Forms.Label();
            this.numericWallY = new System.Windows.Forms.NumericUpDown();
            this.numericWallX = new System.Windows.Forms.NumericUpDown();
            this.buttonAddWall = new System.Windows.Forms.Button();
            this.tablePanelKeyboard = new System.Windows.Forms.TableLayoutPanel();
            this.buttonUp = new System.Windows.Forms.Button();
            this.buttonDown = new System.Windows.Forms.Button();
            this.buttonLeft = new System.Windows.Forms.Button();
            this.buttonRight = new System.Windows.Forms.Button();
            this.tablePanelInfo = new System.Windows.Forms.TableLayoutPanel();
            this.textBoxListFood = new System.Windows.Forms.TextBox();
            this.textBoxListWall = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.barTool.SuspendLayout();
            this.tablePanelMain.SuspendLayout();
            this.tableLayoutFood.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericFoodY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericFoodX)).BeginInit();
            this.tablePanelWall.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericWallY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericWallX)).BeginInit();
            this.tablePanelKeyboard.SuspendLayout();
            this.tablePanelInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // barTool
            // 
            this.barTool.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.barTool.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolButtonRun,
            this.toolButtonStop,
            this.toolButtonRestart,
            this.toolButtonNextStep,
            this.toolStripSeparator2,
            this.toolButtonAddWallFood});
            this.barTool.Location = new System.Drawing.Point(0, 0);
            this.barTool.Name = "barTool";
            this.barTool.Size = new System.Drawing.Size(294, 31);
            this.barTool.TabIndex = 1;
            this.barTool.Text = "toolStrip1";
            // 
            // toolButtonRun
            // 
            this.toolButtonRun.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolButtonRun.Image = ((System.Drawing.Image)(resources.GetObject("toolButtonRun.Image")));
            this.toolButtonRun.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonRun.Name = "toolButtonRun";
            this.toolButtonRun.Size = new System.Drawing.Size(28, 28);
            this.toolButtonRun.Text = "Запуск";
            this.toolButtonRun.Click += new System.EventHandler(this.toolButtonRun_Click);
            // 
            // toolButtonStop
            // 
            this.toolButtonStop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolButtonStop.Image = ((System.Drawing.Image)(resources.GetObject("toolButtonStop.Image")));
            this.toolButtonStop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonStop.Name = "toolButtonStop";
            this.toolButtonStop.Size = new System.Drawing.Size(28, 28);
            this.toolButtonStop.Text = "Стоп";
            this.toolButtonStop.Click += new System.EventHandler(this.toolButtonStop_Click);
            // 
            // toolButtonRestart
            // 
            this.toolButtonRestart.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolButtonRestart.Image = ((System.Drawing.Image)(resources.GetObject("toolButtonRestart.Image")));
            this.toolButtonRestart.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonRestart.Name = "toolButtonRestart";
            this.toolButtonRestart.Size = new System.Drawing.Size(28, 28);
            this.toolButtonRestart.Text = "Рестарт";
            this.toolButtonRestart.Click += new System.EventHandler(this.toolButtonRestart_Click);
            // 
            // toolButtonNextStep
            // 
            this.toolButtonNextStep.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolButtonNextStep.Image = ((System.Drawing.Image)(resources.GetObject("toolButtonNextStep.Image")));
            this.toolButtonNextStep.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonNextStep.Name = "toolButtonNextStep";
            this.toolButtonNextStep.Size = new System.Drawing.Size(28, 28);
            this.toolButtonNextStep.Text = "Следующий шаг";
            this.toolButtonNextStep.Click += new System.EventHandler(this.toolButtonNextStep_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 31);
            // 
            // toolButtonAddWallFood
            // 
            this.toolButtonAddWallFood.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolButtonAddWallFood.Image = ((System.Drawing.Image)(resources.GetObject("toolButtonAddWallFood.Image")));
            this.toolButtonAddWallFood.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonAddWallFood.Name = "toolButtonAddWallFood";
            this.toolButtonAddWallFood.Size = new System.Drawing.Size(28, 28);
            this.toolButtonAddWallFood.Text = "Добавлять на поле стену/еду";
            this.toolButtonAddWallFood.Click += new System.EventHandler(this.toolButtonAddWallFood_Click);
            // 
            // tablePanelMain
            // 
            this.tablePanelMain.ColumnCount = 1;
            this.tablePanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tablePanelMain.Controls.Add(this.tableLayoutFood, 0, 1);
            this.tablePanelMain.Controls.Add(this.tablePanelWall, 0, 0);
            this.tablePanelMain.Controls.Add(this.tablePanelKeyboard, 0, 2);
            this.tablePanelMain.Controls.Add(this.tablePanelInfo, 0, 3);
            this.tablePanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanelMain.Location = new System.Drawing.Point(0, 31);
            this.tablePanelMain.Name = "tablePanelMain";
            this.tablePanelMain.RowCount = 4;
            this.tablePanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tablePanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tablePanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tablePanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tablePanelMain.Size = new System.Drawing.Size(294, 437);
            this.tablePanelMain.TabIndex = 2;
            // 
            // tableLayoutFood
            // 
            this.tableLayoutFood.ColumnCount = 6;
            this.tableLayoutFood.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutFood.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutFood.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutFood.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutFood.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutFood.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutFood.Controls.Add(this.labelAddFood, 0, 0);
            this.tableLayoutFood.Controls.Add(this.labelFoodX, 1, 0);
            this.tableLayoutFood.Controls.Add(this.labelFoodY, 3, 0);
            this.tableLayoutFood.Controls.Add(this.numericFoodY, 4, 0);
            this.tableLayoutFood.Controls.Add(this.numericFoodX, 2, 0);
            this.tableLayoutFood.Controls.Add(this.buttonAddFood, 5, 0);
            this.tableLayoutFood.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutFood.Location = new System.Drawing.Point(3, 46);
            this.tableLayoutFood.Name = "tableLayoutFood";
            this.tableLayoutFood.RowCount = 1;
            this.tableLayoutFood.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutFood.Size = new System.Drawing.Size(288, 37);
            this.tableLayoutFood.TabIndex = 1;
            // 
            // labelAddFood
            // 
            this.labelAddFood.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelAddFood.AutoSize = true;
            this.labelAddFood.Location = new System.Drawing.Point(3, 0);
            this.labelAddFood.Name = "labelAddFood";
            this.labelAddFood.Size = new System.Drawing.Size(66, 37);
            this.labelAddFood.TabIndex = 0;
            this.labelAddFood.Text = "Добавить еду:";
            this.labelAddFood.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelFoodX
            // 
            this.labelFoodX.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelFoodX.AutoSize = true;
            this.labelFoodX.Location = new System.Drawing.Point(75, 0);
            this.labelFoodX.Name = "labelFoodX";
            this.labelFoodX.Size = new System.Drawing.Size(37, 37);
            this.labelFoodX.TabIndex = 1;
            this.labelFoodX.Text = "X";
            this.labelFoodX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelFoodY
            // 
            this.labelFoodY.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelFoodY.AutoSize = true;
            this.labelFoodY.Location = new System.Drawing.Point(161, 0);
            this.labelFoodY.Name = "labelFoodY";
            this.labelFoodY.Size = new System.Drawing.Size(37, 37);
            this.labelFoodY.TabIndex = 2;
            this.labelFoodY.Text = "Y";
            this.labelFoodY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numericFoodY
            // 
            this.numericFoodY.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numericFoodY.Location = new System.Drawing.Point(204, 8);
            this.numericFoodY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericFoodY.Name = "numericFoodY";
            this.numericFoodY.Size = new System.Drawing.Size(37, 20);
            this.numericFoodY.TabIndex = 4;
            this.numericFoodY.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericFoodX
            // 
            this.numericFoodX.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numericFoodX.Location = new System.Drawing.Point(118, 8);
            this.numericFoodX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericFoodX.Name = "numericFoodX";
            this.numericFoodX.Size = new System.Drawing.Size(37, 20);
            this.numericFoodX.TabIndex = 3;
            this.numericFoodX.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // buttonAddFood
            // 
            this.buttonAddFood.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAddFood.Location = new System.Drawing.Point(247, 7);
            this.buttonAddFood.Name = "buttonAddFood";
            this.buttonAddFood.Size = new System.Drawing.Size(38, 23);
            this.buttonAddFood.TabIndex = 5;
            this.buttonAddFood.Text = "+";
            this.buttonAddFood.UseVisualStyleBackColor = true;
            this.buttonAddFood.Click += new System.EventHandler(this.buttonAddFood_Click);
            // 
            // tablePanelWall
            // 
            this.tablePanelWall.ColumnCount = 6;
            this.tablePanelWall.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tablePanelWall.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tablePanelWall.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tablePanelWall.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tablePanelWall.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tablePanelWall.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tablePanelWall.Controls.Add(this.labelAddWall, 0, 0);
            this.tablePanelWall.Controls.Add(this.labelWallX, 1, 0);
            this.tablePanelWall.Controls.Add(this.labelWallY, 3, 0);
            this.tablePanelWall.Controls.Add(this.numericWallY, 4, 0);
            this.tablePanelWall.Controls.Add(this.numericWallX, 2, 0);
            this.tablePanelWall.Controls.Add(this.buttonAddWall, 5, 0);
            this.tablePanelWall.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanelWall.Location = new System.Drawing.Point(3, 3);
            this.tablePanelWall.Name = "tablePanelWall";
            this.tablePanelWall.RowCount = 1;
            this.tablePanelWall.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tablePanelWall.Size = new System.Drawing.Size(288, 37);
            this.tablePanelWall.TabIndex = 0;
            // 
            // labelAddWall
            // 
            this.labelAddWall.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelAddWall.AutoSize = true;
            this.labelAddWall.Location = new System.Drawing.Point(3, 0);
            this.labelAddWall.Name = "labelAddWall";
            this.labelAddWall.Size = new System.Drawing.Size(66, 37);
            this.labelAddWall.TabIndex = 0;
            this.labelAddWall.Text = "Добавить стену:";
            this.labelAddWall.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelWallX
            // 
            this.labelWallX.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelWallX.AutoSize = true;
            this.labelWallX.Location = new System.Drawing.Point(75, 0);
            this.labelWallX.Name = "labelWallX";
            this.labelWallX.Size = new System.Drawing.Size(37, 37);
            this.labelWallX.TabIndex = 1;
            this.labelWallX.Text = "X";
            this.labelWallX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelWallY
            // 
            this.labelWallY.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelWallY.AutoSize = true;
            this.labelWallY.Location = new System.Drawing.Point(161, 0);
            this.labelWallY.Name = "labelWallY";
            this.labelWallY.Size = new System.Drawing.Size(37, 37);
            this.labelWallY.TabIndex = 2;
            this.labelWallY.Text = "Y";
            this.labelWallY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numericWallY
            // 
            this.numericWallY.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numericWallY.Location = new System.Drawing.Point(204, 8);
            this.numericWallY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericWallY.Name = "numericWallY";
            this.numericWallY.Size = new System.Drawing.Size(37, 20);
            this.numericWallY.TabIndex = 4;
            this.numericWallY.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericWallX
            // 
            this.numericWallX.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numericWallX.Location = new System.Drawing.Point(118, 8);
            this.numericWallX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericWallX.Name = "numericWallX";
            this.numericWallX.Size = new System.Drawing.Size(37, 20);
            this.numericWallX.TabIndex = 3;
            this.numericWallX.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // buttonAddWall
            // 
            this.buttonAddWall.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAddWall.Location = new System.Drawing.Point(247, 7);
            this.buttonAddWall.Name = "buttonAddWall";
            this.buttonAddWall.Size = new System.Drawing.Size(38, 23);
            this.buttonAddWall.TabIndex = 5;
            this.buttonAddWall.Text = "+";
            this.buttonAddWall.UseVisualStyleBackColor = true;
            this.buttonAddWall.Click += new System.EventHandler(this.buttonAddWall_Click);
            // 
            // tablePanelKeyboard
            // 
            this.tablePanelKeyboard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.tablePanelKeyboard.ColumnCount = 3;
            this.tablePanelKeyboard.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tablePanelKeyboard.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tablePanelKeyboard.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tablePanelKeyboard.Controls.Add(this.buttonUp, 1, 0);
            this.tablePanelKeyboard.Controls.Add(this.buttonDown, 1, 1);
            this.tablePanelKeyboard.Controls.Add(this.buttonLeft, 0, 0);
            this.tablePanelKeyboard.Controls.Add(this.buttonRight, 2, 0);
            this.tablePanelKeyboard.Location = new System.Drawing.Point(47, 89);
            this.tablePanelKeyboard.Name = "tablePanelKeyboard";
            this.tablePanelKeyboard.RowCount = 2;
            this.tablePanelKeyboard.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tablePanelKeyboard.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tablePanelKeyboard.Size = new System.Drawing.Size(200, 81);
            this.tablePanelKeyboard.TabIndex = 2;
            // 
            // buttonUp
            // 
            this.buttonUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.buttonUp.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonUp.BackgroundImage")));
            this.buttonUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonUp.Location = new System.Drawing.Point(69, 3);
            this.buttonUp.Name = "buttonUp";
            this.buttonUp.Size = new System.Drawing.Size(60, 34);
            this.buttonUp.TabIndex = 0;
            this.buttonUp.UseVisualStyleBackColor = true;
            this.buttonUp.Click += new System.EventHandler(this.buttonUp_Click);
            // 
            // buttonDown
            // 
            this.buttonDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.buttonDown.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonDown.BackgroundImage")));
            this.buttonDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonDown.Location = new System.Drawing.Point(69, 43);
            this.buttonDown.Name = "buttonDown";
            this.buttonDown.Size = new System.Drawing.Size(60, 35);
            this.buttonDown.TabIndex = 1;
            this.buttonDown.UseVisualStyleBackColor = true;
            this.buttonDown.Click += new System.EventHandler(this.buttonDown_Click);
            // 
            // buttonLeft
            // 
            this.buttonLeft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.buttonLeft.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonLeft.BackgroundImage")));
            this.buttonLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonLeft.Location = new System.Drawing.Point(3, 3);
            this.buttonLeft.Name = "buttonLeft";
            this.tablePanelKeyboard.SetRowSpan(this.buttonLeft, 2);
            this.buttonLeft.Size = new System.Drawing.Size(60, 75);
            this.buttonLeft.TabIndex = 2;
            this.buttonLeft.UseVisualStyleBackColor = true;
            this.buttonLeft.Click += new System.EventHandler(this.buttonLeft_Click);
            // 
            // buttonRight
            // 
            this.buttonRight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.buttonRight.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonRight.BackgroundImage")));
            this.buttonRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonRight.Location = new System.Drawing.Point(135, 3);
            this.buttonRight.Name = "buttonRight";
            this.tablePanelKeyboard.SetRowSpan(this.buttonRight, 2);
            this.buttonRight.Size = new System.Drawing.Size(62, 75);
            this.buttonRight.TabIndex = 3;
            this.buttonRight.UseVisualStyleBackColor = true;
            this.buttonRight.Click += new System.EventHandler(this.buttonRight_Click);
            // 
            // tablePanelInfo
            // 
            this.tablePanelInfo.ColumnCount = 2;
            this.tablePanelInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tablePanelInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tablePanelInfo.Controls.Add(this.textBoxListFood, 1, 1);
            this.tablePanelInfo.Controls.Add(this.textBoxListWall, 0, 1);
            this.tablePanelInfo.Controls.Add(this.label2, 1, 0);
            this.tablePanelInfo.Controls.Add(this.label1, 0, 0);
            this.tablePanelInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanelInfo.Location = new System.Drawing.Point(3, 176);
            this.tablePanelInfo.Name = "tablePanelInfo";
            this.tablePanelInfo.RowCount = 2;
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tablePanelInfo.Size = new System.Drawing.Size(288, 258);
            this.tablePanelInfo.TabIndex = 3;
            // 
            // textBoxListFood
            // 
            this.textBoxListFood.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxListFood.Location = new System.Drawing.Point(147, 28);
            this.textBoxListFood.Multiline = true;
            this.textBoxListFood.Name = "textBoxListFood";
            this.textBoxListFood.ReadOnly = true;
            this.textBoxListFood.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxListFood.Size = new System.Drawing.Size(138, 227);
            this.textBoxListFood.TabIndex = 4;
            this.textBoxListFood.TabStop = false;
            // 
            // textBoxListWall
            // 
            this.textBoxListWall.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxListWall.Location = new System.Drawing.Point(3, 28);
            this.textBoxListWall.Multiline = true;
            this.textBoxListWall.Name = "textBoxListWall";
            this.textBoxListWall.ReadOnly = true;
            this.textBoxListWall.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxListWall.Size = new System.Drawing.Size(138, 227);
            this.textBoxListWall.TabIndex = 3;
            this.textBoxListWall.TabStop = false;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(147, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Еда (координаты):";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Стена (координаты):";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer
            // 
            this.timer.Interval = 500;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // Task
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(294, 468);
            this.Controls.Add(this.tablePanelMain);
            this.Controls.Add(this.barTool);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Task";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Ввод данных программы";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Task_FormClosing);
            this.barTool.ResumeLayout(false);
            this.barTool.PerformLayout();
            this.tablePanelMain.ResumeLayout(false);
            this.tableLayoutFood.ResumeLayout(false);
            this.tableLayoutFood.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericFoodY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericFoodX)).EndInit();
            this.tablePanelWall.ResumeLayout(false);
            this.tablePanelWall.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericWallY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericWallX)).EndInit();
            this.tablePanelKeyboard.ResumeLayout(false);
            this.tablePanelInfo.ResumeLayout(false);
            this.tablePanelInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip barTool;
        private System.Windows.Forms.ToolStripButton toolButtonRun;
        private System.Windows.Forms.ToolStripButton toolButtonStop;
        private System.Windows.Forms.ToolStripButton toolButtonNextStep;
        private System.Windows.Forms.TableLayoutPanel tablePanelMain;
        private System.Windows.Forms.TableLayoutPanel tablePanelWall;
        private System.Windows.Forms.Label labelAddWall;
        private System.Windows.Forms.Label labelWallX;
        private System.Windows.Forms.Label labelWallY;
        private System.Windows.Forms.NumericUpDown numericWallX;
        private System.Windows.Forms.NumericUpDown numericWallY;
        private System.Windows.Forms.Button buttonAddWall;
        private System.Windows.Forms.TableLayoutPanel tableLayoutFood;
        private System.Windows.Forms.Label labelAddFood;
        private System.Windows.Forms.Label labelFoodX;
        private System.Windows.Forms.Label labelFoodY;
        private System.Windows.Forms.NumericUpDown numericFoodY;
        private System.Windows.Forms.NumericUpDown numericFoodX;
        private System.Windows.Forms.Button buttonAddFood;
        private System.Windows.Forms.TableLayoutPanel tablePanelKeyboard;
        private System.Windows.Forms.Button buttonUp;
        private System.Windows.Forms.Button buttonDown;
        private System.Windows.Forms.Button buttonLeft;
        private System.Windows.Forms.Button buttonRight;
        private System.Windows.Forms.TableLayoutPanel tablePanelInfo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxListWall;
        private System.Windows.Forms.TextBox textBoxListFood;
        private System.Windows.Forms.ToolStripButton toolButtonAddWallFood;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.ToolStripButton toolButtonRestart;
    }
}