﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication_Sixth
{
    public partial class Grid : Control
    {
        public Grid()
        {
            InitializeComponent();

            rowCount = Data.Instance.RowCount;
            columnCount = Data.Instance.ColumnCount;
        }

        int cellSize = 25;
        int rowCount, columnCount;

        Pen gridPen = new Pen(Color.Black, 1);
        [DefaultValue(null)]
        public Color LineColor
        {
            set
            {
                gridPen.Color = value;
                this.Refresh();
            }
            get
            {
                return gridPen.Color;
            }
        }
        SolidBrush gridBrush = new SolidBrush(Color.White);
        [DefaultValue(null)]
        public Color BrushColor
        {
            set
            {
                gridBrush.Color = value;
                this.Refresh();
            }
            get
            {
                return gridBrush.Color;
            }
        }

        public delegate void CellHandler(Object sender, int column, int row);
        public event CellHandler CellClick;
        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);

            if (ContainsPoint(e.Location))
                if (CellClick != null)
                    CellClick(this, e.X / cellSize, e.Y / cellSize);
        }
        public bool ContainsPoint(Point point)
        {
            if ((point.X >= 0) && (point.X <= cellSize * columnCount) && (point.Y >= 0) && (point.Y <= cellSize * rowCount))
                return true;
            else
                return false;
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);

            Graphics place = pe.Graphics;

            // меняем размер клетки в зависимости от размера сетки
            if (this.Size.Width > this.Size.Height)
                cellSize = this.Size.Width / columnCount;
            else
                cellSize = this.Size.Height / rowCount;

            int widthGrid = columnCount * cellSize;
            int heightGrid = rowCount * cellSize;

            // строим сетку
            for (int i = 0; i <= widthGrid; i += cellSize)
                place.DrawLine(gridPen, i, 0, i, heightGrid);
            for (int i = 0; i <= heightGrid; i += cellSize)
                place.DrawLine(gridPen, 0, i, widthGrid, i);

            // очищаем сетку, попутно расставляя стены и еду
            for (int i = 0; i < columnCount; i++)
                for (int j = 0; j < rowCount; j++)
                    {
                        if (Data.Instance.GetWall(i, j) == 1)
                            place.FillRectangle(Brushes.Black, i * cellSize + 1, j * cellSize + 1, cellSize - 1, cellSize - 1);
                        else if (Data.Instance.GetFood(i, j) == 1)
                            place.FillRectangle(Brushes.Red, i * cellSize + 1, j * cellSize + 1, cellSize - 1, cellSize - 1);
                        else
                            place.FillRectangle(Brushes.White, i * cellSize + 1, j * cellSize + 1, cellSize - 1, cellSize - 1);
                    }

            // рисуем змейку в сетке
            List<Point> tempList = Data.Instance.Snake.ToList<Point>();
            if (tempList != null)
                for (int i = 0; i < tempList.Count; i++)
                {
                    Point tempPoint = tempList[i];
                    place.FillRectangle(Brushes.Yellow, tempPoint.X * cellSize + 1, tempPoint.Y * cellSize + 1, cellSize - 1, cellSize - 1);
                }
        }
    }
}
