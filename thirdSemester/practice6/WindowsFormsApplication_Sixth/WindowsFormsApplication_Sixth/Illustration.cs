﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication_Sixth
{
    public partial class Illustration : Form
    {
        public Illustration()
        {
            InitializeComponent();

            // регистрируем обработчик событий обновления окна формы при изменении или очищении данных
            Data.Instance.DataChanged += UpdateInformation;
            Data.Instance.DataCleared += UpdateInformation;
        }

        // при закрытии не уничтожает окно
        private void Illustration_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        // перерисовка главного окна приложения
        private void Illustration_Resize(object sender, EventArgs e)
        {
            grid.Refresh();
        }

        // обновление данных в окне
        private void UpdateInformation(object sender, DataEventArgs e)
        {
            barStatusLabel.Text = e.message;
            grid.Refresh();
        }

        // добавление стены или еды на поле
        private void grid_CellClick(object sender, int column, int row)
        {
            Data.Instance.AddWallFood(column, row);
        }
    }
}
