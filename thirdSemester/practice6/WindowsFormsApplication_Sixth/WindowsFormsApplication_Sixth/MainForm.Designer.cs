﻿namespace WindowsFormsApplication_Sixth
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuProgram = new System.Windows.Forms.MenuStrip();
            this.menuSectionFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemCreate = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemSaveFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemExit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSectionWork = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemTask = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemDataStructure = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemIllustration = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSectionHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemDescription = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.barTool = new System.Windows.Forms.ToolStrip();
            this.toolButtonCreate = new System.Windows.Forms.ToolStripButton();
            this.toolButtonSaveFile = new System.Windows.Forms.ToolStripButton();
            this.toolButtonExit = new System.Windows.Forms.ToolStripButton();
            this.toolSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolButtonTask = new System.Windows.Forms.ToolStripButton();
            this.toolButtonDataStructure = new System.Windows.Forms.ToolStripButton();
            this.toolButtonIllustration = new System.Windows.Forms.ToolStripButton();
            this.toolSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolButtonDescription = new System.Windows.Forms.ToolStripButton();
            this.toolButtonAbout = new System.Windows.Forms.ToolStripButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.menuProgram.SuspendLayout();
            this.barTool.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuProgram
            // 
            this.menuProgram.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuSectionFile,
            this.menuSectionWork,
            this.menuSectionHelp});
            this.menuProgram.Location = new System.Drawing.Point(0, 0);
            this.menuProgram.Name = "menuProgram";
            this.menuProgram.Size = new System.Drawing.Size(492, 24);
            this.menuProgram.TabIndex = 0;
            this.menuProgram.Text = "Главное меню программы";
            // 
            // menuSectionFile
            // 
            this.menuSectionFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemCreate,
            this.menuItemSaveFile,
            this.menuItemExit});
            this.menuSectionFile.Name = "menuSectionFile";
            this.menuSectionFile.Size = new System.Drawing.Size(45, 20);
            this.menuSectionFile.Text = "Файл";
            // 
            // menuItemCreate
            // 
            this.menuItemCreate.Name = "menuItemCreate";
            this.menuItemCreate.Size = new System.Drawing.Size(178, 22);
            this.menuItemCreate.Text = "Создать";
            this.menuItemCreate.Click += new System.EventHandler(this.menuItemCreate_Click);
            // 
            // menuItemSaveFile
            // 
            this.menuItemSaveFile.Name = "menuItemSaveFile";
            this.menuItemSaveFile.Size = new System.Drawing.Size(178, 22);
            this.menuItemSaveFile.Text = "Сохранить в файл";
            this.menuItemSaveFile.Click += new System.EventHandler(this.menuItemSaveFile_Click);
            // 
            // menuItemExit
            // 
            this.menuItemExit.Name = "menuItemExit";
            this.menuItemExit.Size = new System.Drawing.Size(178, 22);
            this.menuItemExit.Text = "Выход";
            this.menuItemExit.Click += new System.EventHandler(this.menuItemExit_Click);
            // 
            // menuSectionWork
            // 
            this.menuSectionWork.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemTask,
            this.menuItemDataStructure,
            this.menuItemIllustration});
            this.menuSectionWork.Name = "menuSectionWork";
            this.menuSectionWork.Size = new System.Drawing.Size(55, 20);
            this.menuSectionWork.Text = "Работа";
            // 
            // menuItemTask
            // 
            this.menuItemTask.Name = "menuItemTask";
            this.menuItemTask.Size = new System.Drawing.Size(184, 22);
            this.menuItemTask.Text = "Задача";
            this.menuItemTask.Click += new System.EventHandler(this.menuItemTask_Click);
            // 
            // menuItemDataStructure
            // 
            this.menuItemDataStructure.Name = "menuItemDataStructure";
            this.menuItemDataStructure.Size = new System.Drawing.Size(184, 22);
            this.menuItemDataStructure.Text = "Структуры данных";
            this.menuItemDataStructure.Click += new System.EventHandler(this.menuItemDataStructure_Click);
            // 
            // menuItemIllustration
            // 
            this.menuItemIllustration.Name = "menuItemIllustration";
            this.menuItemIllustration.Size = new System.Drawing.Size(184, 22);
            this.menuItemIllustration.Text = "Иллюстрация";
            this.menuItemIllustration.Click += new System.EventHandler(this.menuItemIllustration_Click);
            // 
            // menuSectionHelp
            // 
            this.menuSectionHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemDescription,
            this.menuItemAbout});
            this.menuSectionHelp.Name = "menuSectionHelp";
            this.menuSectionHelp.Size = new System.Drawing.Size(62, 20);
            this.menuSectionHelp.Text = "Справка";
            // 
            // menuItemDescription
            // 
            this.menuItemDescription.Name = "menuItemDescription";
            this.menuItemDescription.Size = new System.Drawing.Size(149, 22);
            this.menuItemDescription.Text = "Описание";
            this.menuItemDescription.Click += new System.EventHandler(this.menuItemDescription_Click);
            // 
            // menuItemAbout
            // 
            this.menuItemAbout.Name = "menuItemAbout";
            this.menuItemAbout.Size = new System.Drawing.Size(149, 22);
            this.menuItemAbout.Text = "О программе";
            this.menuItemAbout.Click += new System.EventHandler(this.menuItemAbout_Click);
            // 
            // barTool
            // 
            this.barTool.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.barTool.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolButtonCreate,
            this.toolButtonSaveFile,
            this.toolButtonExit,
            this.toolSeparator1,
            this.toolButtonTask,
            this.toolButtonDataStructure,
            this.toolButtonIllustration,
            this.toolSeparator2,
            this.toolButtonDescription,
            this.toolButtonAbout});
            this.barTool.Location = new System.Drawing.Point(0, 24);
            this.barTool.Name = "barTool";
            this.barTool.Size = new System.Drawing.Size(492, 31);
            this.barTool.TabIndex = 2;
            this.barTool.Text = "Панель инструментов";
            // 
            // toolButtonCreate
            // 
            this.toolButtonCreate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolButtonCreate.Image = ((System.Drawing.Image)(resources.GetObject("toolButtonCreate.Image")));
            this.toolButtonCreate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonCreate.Name = "toolButtonCreate";
            this.toolButtonCreate.Size = new System.Drawing.Size(28, 28);
            this.toolButtonCreate.Text = "Создать";
            this.toolButtonCreate.Click += new System.EventHandler(this.menuItemCreate_Click);
            // 
            // toolButtonSaveFile
            // 
            this.toolButtonSaveFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolButtonSaveFile.Image = ((System.Drawing.Image)(resources.GetObject("toolButtonSaveFile.Image")));
            this.toolButtonSaveFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonSaveFile.Name = "toolButtonSaveFile";
            this.toolButtonSaveFile.Size = new System.Drawing.Size(28, 28);
            this.toolButtonSaveFile.Text = "Сохранить в файл";
            this.toolButtonSaveFile.Click += new System.EventHandler(this.menuItemSaveFile_Click);
            // 
            // toolButtonExit
            // 
            this.toolButtonExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolButtonExit.Image = ((System.Drawing.Image)(resources.GetObject("toolButtonExit.Image")));
            this.toolButtonExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonExit.Name = "toolButtonExit";
            this.toolButtonExit.Size = new System.Drawing.Size(28, 28);
            this.toolButtonExit.Text = "Выход";
            this.toolButtonExit.Click += new System.EventHandler(this.menuItemExit_Click);
            // 
            // toolSeparator1
            // 
            this.toolSeparator1.Name = "toolSeparator1";
            this.toolSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // toolButtonTask
            // 
            this.toolButtonTask.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolButtonTask.Image = ((System.Drawing.Image)(resources.GetObject("toolButtonTask.Image")));
            this.toolButtonTask.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonTask.Name = "toolButtonTask";
            this.toolButtonTask.Size = new System.Drawing.Size(28, 28);
            this.toolButtonTask.Text = "Задача";
            this.toolButtonTask.Click += new System.EventHandler(this.menuItemTask_Click);
            // 
            // toolButtonDataStructure
            // 
            this.toolButtonDataStructure.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolButtonDataStructure.Image = ((System.Drawing.Image)(resources.GetObject("toolButtonDataStructure.Image")));
            this.toolButtonDataStructure.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonDataStructure.Name = "toolButtonDataStructure";
            this.toolButtonDataStructure.Size = new System.Drawing.Size(28, 28);
            this.toolButtonDataStructure.Text = "Структуры данных";
            this.toolButtonDataStructure.Click += new System.EventHandler(this.menuItemDataStructure_Click);
            // 
            // toolButtonIllustration
            // 
            this.toolButtonIllustration.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolButtonIllustration.Image = ((System.Drawing.Image)(resources.GetObject("toolButtonIllustration.Image")));
            this.toolButtonIllustration.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonIllustration.Name = "toolButtonIllustration";
            this.toolButtonIllustration.Size = new System.Drawing.Size(28, 28);
            this.toolButtonIllustration.Text = "Иллюстрация";
            this.toolButtonIllustration.Click += new System.EventHandler(this.menuItemIllustration_Click);
            // 
            // toolSeparator2
            // 
            this.toolSeparator2.Name = "toolSeparator2";
            this.toolSeparator2.Size = new System.Drawing.Size(6, 31);
            // 
            // toolButtonDescription
            // 
            this.toolButtonDescription.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolButtonDescription.Image = ((System.Drawing.Image)(resources.GetObject("toolButtonDescription.Image")));
            this.toolButtonDescription.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonDescription.Name = "toolButtonDescription";
            this.toolButtonDescription.Size = new System.Drawing.Size(28, 28);
            this.toolButtonDescription.Text = "Описание";
            this.toolButtonDescription.Click += new System.EventHandler(this.menuItemDescription_Click);
            // 
            // toolButtonAbout
            // 
            this.toolButtonAbout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolButtonAbout.Image = ((System.Drawing.Image)(resources.GetObject("toolButtonAbout.Image")));
            this.toolButtonAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonAbout.Name = "toolButtonAbout";
            this.toolButtonAbout.Size = new System.Drawing.Size(28, 28);
            this.toolButtonAbout.Text = "О программе";
            this.toolButtonAbout.Click += new System.EventHandler(this.menuItemAbout_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(0, 55);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(492, 311);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "Text files|*.txt|All fles|*.*";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 366);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.barTool);
            this.Controls.Add(this.menuProgram);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MainMenuStrip = this.menuProgram;
            this.Name = "MainForm";
            this.Text = "Учебная компьютерная игра \"Змейка\"";
            this.menuProgram.ResumeLayout(false);
            this.menuProgram.PerformLayout();
            this.barTool.ResumeLayout(false);
            this.barTool.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuProgram;
        private System.Windows.Forms.ToolStripMenuItem menuSectionFile;
        private System.Windows.Forms.ToolStripMenuItem menuItemCreate;
        private System.Windows.Forms.ToolStripMenuItem menuItemSaveFile;
        private System.Windows.Forms.ToolStripMenuItem menuItemExit;
        private System.Windows.Forms.ToolStripMenuItem menuSectionWork;
        private System.Windows.Forms.ToolStripMenuItem menuItemTask;
        private System.Windows.Forms.ToolStripMenuItem menuItemDataStructure;
        private System.Windows.Forms.ToolStripMenuItem menuSectionHelp;
        private System.Windows.Forms.ToolStripMenuItem menuItemDescription;
        private System.Windows.Forms.ToolStripMenuItem menuItemAbout;
        private System.Windows.Forms.ToolStrip barTool;
        private System.Windows.Forms.ToolStripButton toolButtonCreate;
        private System.Windows.Forms.ToolStripButton toolButtonSaveFile;
        private System.Windows.Forms.ToolStripButton toolButtonExit;
        private System.Windows.Forms.ToolStripSeparator toolSeparator1;
        private System.Windows.Forms.ToolStripButton toolButtonTask;
        private System.Windows.Forms.ToolStripButton toolButtonDataStructure;
        private System.Windows.Forms.ToolStripSeparator toolSeparator2;
        private System.Windows.Forms.ToolStripButton toolButtonDescription;
        private System.Windows.Forms.ToolStripButton toolButtonAbout;
        private System.Windows.Forms.ToolStripMenuItem menuItemIllustration;
        private System.Windows.Forms.ToolStripButton toolButtonIllustration;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
    }
}