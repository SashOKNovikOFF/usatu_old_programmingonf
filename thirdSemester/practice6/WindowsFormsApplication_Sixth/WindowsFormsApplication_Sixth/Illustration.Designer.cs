﻿namespace WindowsFormsApplication_Sixth
{
    partial class Illustration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.barStatus = new System.Windows.Forms.StatusStrip();
            this.barStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.grid = new WindowsFormsApplication_Sixth.Grid();
            this.barStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // barStatus
            // 
            this.barStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.barStatusLabel});
            this.barStatus.Location = new System.Drawing.Point(0, 344);
            this.barStatus.Name = "barStatus";
            this.barStatus.Size = new System.Drawing.Size(572, 22);
            this.barStatus.TabIndex = 0;
            this.barStatus.Text = "Строка состояния";
            // 
            // barStatusLabel
            // 
            this.barStatusLabel.Name = "barStatusLabel";
            this.barStatusLabel.Size = new System.Drawing.Size(106, 17);
            this.barStatusLabel.Text = "Строка состояния";
            // 
            // grid
            // 
            this.grid.BrushColor = System.Drawing.Color.White;
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.LineColor = System.Drawing.Color.Black;
            this.grid.Location = new System.Drawing.Point(0, 0);
            this.grid.Name = "grid";
            this.grid.Size = new System.Drawing.Size(572, 344);
            this.grid.TabIndex = 4;
            this.grid.Text = "Сетка";
            this.grid.CellClick += new WindowsFormsApplication_Sixth.Grid.CellHandler(this.grid_CellClick);
            // 
            // Illustration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(572, 366);
            this.Controls.Add(this.grid);
            this.Controls.Add(this.barStatus);
            this.MaximumSize = new System.Drawing.Size(900, 600);
            this.MinimumSize = new System.Drawing.Size(500, 400);
            this.Name = "Illustration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Иллюстрация";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Illustration_FormClosing);
            this.Resize += new System.EventHandler(this.Illustration_Resize);
            this.barStatus.ResumeLayout(false);
            this.barStatus.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip barStatus;
        private System.Windows.Forms.ToolStripStatusLabel barStatusLabel;
        private Grid grid;

    }
}