﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO;

namespace WindowsFormsApplication_Sixth
{
    public partial class MainForm : Form
    {
        Task windowTask;
        Illustration windowIllustration;
        DataStructure windowDataStructure;

        public MainForm()
        {
            InitializeComponent();

            windowTask = new Task();
            windowIllustration = new Illustration();
            windowDataStructure = new DataStructure();
        }

        /* Раздел "Файл" */

        // "Создать"
        private void menuItemCreate_Click(object sender, EventArgs e)
        {
            if (!Data.Instance.IsDataOld)
            {
                string tempMessage = "Внесите данные в программу или запустите игру (раздел \"Работа\", пункт \"Задача\").";
                DialogResult warningMSGBox = MessageBox.Show(tempMessage, "Создать новый файл", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                // предложить пользователю выйти из программы
                DialogResult saveMSGBox = MessageBox.Show("Хотите сохранить предыдущие изменения?", "Сброс данных", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

                if (saveMSGBox == DialogResult.Yes)
                {
                    Data.Instance.SaveData();
                    Data.Instance.ClearData();

                    string tempMessage = "Данные были успешно сохранены.";
                    DialogResult warningMSGBox = MessageBox.Show(tempMessage, "Создать новый файл", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (saveMSGBox == DialogResult.No)
                    Data.Instance.ClearData();
            }
        }

        // "Сохранить в файл"
        private void menuItemSaveFile_Click(object sender, EventArgs e)
        {
            if (!Data.Instance.IsDataOld)
            {
                string tempMessage = "Внесите данные в программу или запустите игру (раздел \"Работа\", пункт \"Задача\").";
                DialogResult warningMSGBox = MessageBox.Show(tempMessage, "Сохранить в файл", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                Data.Instance.SaveData();
                string tempMessage = "Данные были успешно сохранены.";
                DialogResult warningMSGBox = MessageBox.Show(tempMessage, "Сохранить в файл", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        // "Выход"
        private void menuItemExit_Click(object sender, EventArgs e)
        {
            // предложить пользователю выйти из программы
            DialogResult exitMSGBox = MessageBox.Show("Вы действительно хотите выйти?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Hand);

            // если пользователь выбрал ответ "Да"
            if (exitMSGBox == DialogResult.Yes)
            {
                windowTask.Dispose();
                windowDataStructure.Dispose();
                windowIllustration.Dispose();
                Application.Exit();
            }
        }

        /* Раздел "Работа" */

        // вызываем окно "Задача"
        private void menuItemTask_Click(object sender, EventArgs e)
        {
            windowTask.Show();
        }

        // вызываем окно "Структура данных"
        private void menuItemDataStructure_Click(object sender, EventArgs e)
        {
            windowDataStructure.Show();
        }

        // вызываем окно "Иллюстрация"
        private void menuItemIllustration_Click(object sender, EventArgs e)
        {
            windowIllustration.Show();
        }

        /* Раздел "Справка" */

        // вызываем окно "Описание программы"
        private void menuItemDescription_Click(object sender, EventArgs e)
        {
            new Description().ShowDialog();
        }

        // вызываем окно "О программе"
        private void menuItemAbout_Click(object sender, EventArgs e)
        {
            new About().ShowDialog();
        }
    }
}
