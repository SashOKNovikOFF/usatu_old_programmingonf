﻿namespace WindowsFormsApplication_First
{
    partial class WorkForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupStudent = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.textName = new System.Windows.Forms.Label();
            this.radioHard = new System.Windows.Forms.RadioButton();
            this.textFamily = new System.Windows.Forms.Label();
            this.radioSimple = new System.Windows.Forms.RadioButton();
            this.textFathName = new System.Windows.Forms.Label();
            this.comboClass = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupStudent.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupStudent);
            this.splitContainer1.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.richTextBox1);
            this.splitContainer1.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.splitContainer1.Size = new System.Drawing.Size(633, 408);
            this.splitContainer1.SplitterDistance = 249;
            this.splitContainer1.TabIndex = 0;
            // 
            // groupStudent
            // 
            this.groupStudent.Controls.Add(this.tableLayoutPanel1);
            this.groupStudent.Cursor = System.Windows.Forms.Cursors.Hand;
            this.groupStudent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupStudent.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupStudent.Location = new System.Drawing.Point(0, 0);
            this.groupStudent.Name = "groupStudent";
            this.groupStudent.Size = new System.Drawing.Size(633, 249);
            this.groupStudent.TabIndex = 8;
            this.groupStudent.TabStop = false;
            this.groupStudent.Text = "Ученик";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.41467F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 79.58533F));
            this.tableLayoutPanel1.Controls.Add(this.textName, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radioHard, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.textFamily, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.radioSimple, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.textFathName, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.comboClass, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.textBox1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBox3, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBox2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.button1, 0, 6);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 19);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(627, 227);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // textName
            // 
            this.textName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textName.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textName.Location = new System.Drawing.Point(13, 17);
            this.textName.Name = "textName";
            this.textName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textName.Size = new System.Drawing.Size(117, 15);
            this.textName.TabIndex = 2;
            this.textName.Text = "Имя";
            this.textName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radioHard
            // 
            this.radioHard.AutoSize = true;
            this.radioHard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radioHard.Location = new System.Drawing.Point(13, 163);
            this.radioHard.Name = "radioHard";
            this.radioHard.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radioHard.Size = new System.Drawing.Size(117, 24);
            this.radioHard.TabIndex = 7;
            this.radioHard.TabStop = true;
            this.radioHard.Text = "Сложно";
            this.radioHard.UseVisualStyleBackColor = true;
            // 
            // textFamily
            // 
            this.textFamily.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textFamily.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textFamily.Location = new System.Drawing.Point(13, 47);
            this.textFamily.Name = "textFamily";
            this.textFamily.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textFamily.Size = new System.Drawing.Size(117, 15);
            this.textFamily.TabIndex = 3;
            this.textFamily.Text = "Фамилия";
            this.textFamily.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radioSimple
            // 
            this.radioSimple.AutoSize = true;
            this.radioSimple.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radioSimple.Location = new System.Drawing.Point(13, 133);
            this.radioSimple.Name = "radioSimple";
            this.radioSimple.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radioSimple.Size = new System.Drawing.Size(117, 24);
            this.radioSimple.TabIndex = 6;
            this.radioSimple.TabStop = true;
            this.radioSimple.Text = "Просто";
            this.radioSimple.UseVisualStyleBackColor = true;
            // 
            // textFathName
            // 
            this.textFathName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textFathName.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textFathName.Location = new System.Drawing.Point(13, 77);
            this.textFathName.Name = "textFathName";
            this.textFathName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textFathName.Size = new System.Drawing.Size(117, 15);
            this.textFathName.TabIndex = 5;
            this.textFathName.Text = "Отчество";
            this.textFathName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboClass
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.comboClass, 2);
            this.comboClass.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboClass.FormattingEnabled = true;
            this.comboClass.Items.AddRange(new object[] {
            "5А",
            "5Б",
            "5В",
            "5Г"});
            this.comboClass.Location = new System.Drawing.Point(13, 103);
            this.comboClass.Name = "comboClass";
            this.comboClass.Size = new System.Drawing.Size(601, 23);
            this.comboClass.TabIndex = 4;
            this.comboClass.SelectedIndexChanged += new System.EventHandler(this.comboClass_SelectedIndexChanged);
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Location = new System.Drawing.Point(143, 10);
            this.textBox1.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.textBox1.MaximumSize = new System.Drawing.Size(300, 50);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(300, 23);
            this.textBox1.TabIndex = 1;
            // 
            // textBox3
            // 
            this.textBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox3.Location = new System.Drawing.Point(143, 70);
            this.textBox3.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.textBox3.MaximumSize = new System.Drawing.Size(300, 50);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(300, 23);
            this.textBox3.TabIndex = 3;
            // 
            // textBox2
            // 
            this.textBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox2.Location = new System.Drawing.Point(143, 40);
            this.textBox2.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.textBox2.MaximumSize = new System.Drawing.Size(300, 50);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(300, 23);
            this.textBox2.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.AutoSize = true;
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(13, 193);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(117, 24);
            this.button1.TabIndex = 8;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.richTextBox1.Location = new System.Drawing.Point(0, 0);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(633, 155);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // WorkForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(633, 408);
            this.Controls.Add(this.splitContainer1);
            this.Name = "WorkForm";
            this.Text = "WorkForm";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupStudent.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupStudent;
        private System.Windows.Forms.RadioButton radioHard;
        private System.Windows.Forms.RadioButton radioSimple;
        private System.Windows.Forms.ComboBox comboClass;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label textFathName;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label textFamily;
        private System.Windows.Forms.Label textName;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}