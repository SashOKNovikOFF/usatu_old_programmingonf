﻿namespace WindowsFormsApplication_First
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOK = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textName = new System.Windows.Forms.Label();
            this.textFamily = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textFathName = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.groupStudent = new System.Windows.Forms.GroupBox();
            this.comboClass = new System.Windows.Forms.ComboBox();
            this.radioSimple = new System.Windows.Forms.RadioButton();
            this.radioHard = new System.Windows.Forms.RadioButton();
            this.groupStudent.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(57, 289);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 5;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(77, 14);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 23);
            this.textBox1.TabIndex = 1;
            // 
            // textName
            // 
            this.textName.AutoSize = true;
            this.textName.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textName.Location = new System.Drawing.Point(12, 17);
            this.textName.Name = "textName";
            this.textName.Size = new System.Drawing.Size(29, 15);
            this.textName.TabIndex = 2;
            this.textName.Text = "Имя";
            // 
            // textFamily
            // 
            this.textFamily.AutoSize = true;
            this.textFamily.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textFamily.Location = new System.Drawing.Point(12, 43);
            this.textFamily.Name = "textFamily";
            this.textFamily.Size = new System.Drawing.Size(55, 15);
            this.textFamily.TabIndex = 3;
            this.textFamily.Text = "Фамилия";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(77, 40);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 23);
            this.textBox2.TabIndex = 2;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // textFathName
            // 
            this.textFathName.AutoSize = true;
            this.textFathName.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textFathName.Location = new System.Drawing.Point(12, 69);
            this.textFathName.Name = "textFathName";
            this.textFathName.Size = new System.Drawing.Size(57, 15);
            this.textFathName.TabIndex = 5;
            this.textFathName.Text = "Отчество";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(77, 66);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 23);
            this.textBox3.TabIndex = 3;
            // 
            // groupStudent
            // 
            this.groupStudent.BackColor = System.Drawing.Color.Transparent;
            this.groupStudent.Controls.Add(this.radioHard);
            this.groupStudent.Controls.Add(this.radioSimple);
            this.groupStudent.Controls.Add(this.comboClass);
            this.groupStudent.Controls.Add(this.textBox3);
            this.groupStudent.Controls.Add(this.textFathName);
            this.groupStudent.Controls.Add(this.textBox2);
            this.groupStudent.Controls.Add(this.textFamily);
            this.groupStudent.Controls.Add(this.textName);
            this.groupStudent.Controls.Add(this.textBox1);
            this.groupStudent.Cursor = System.Windows.Forms.Cursors.Hand;
            this.groupStudent.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupStudent.Location = new System.Drawing.Point(29, 35);
            this.groupStudent.Name = "groupStudent";
            this.groupStudent.Size = new System.Drawing.Size(264, 202);
            this.groupStudent.TabIndex = 7;
            this.groupStudent.TabStop = false;
            this.groupStudent.Text = "Ученик";
            // 
            // comboClass
            // 
            this.comboClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboClass.FormattingEnabled = true;
            this.comboClass.Items.AddRange(new object[] {
            "5А",
            "5Б",
            "5В",
            "5Г"});
            this.comboClass.Location = new System.Drawing.Point(15, 101);
            this.comboClass.Name = "comboClass";
            this.comboClass.Size = new System.Drawing.Size(162, 23);
            this.comboClass.TabIndex = 4;
            // 
            // radioSimple
            // 
            this.radioSimple.AutoSize = true;
            this.radioSimple.Location = new System.Drawing.Point(15, 130);
            this.radioSimple.Name = "radioSimple";
            this.radioSimple.Size = new System.Drawing.Size(66, 19);
            this.radioSimple.TabIndex = 6;
            this.radioSimple.TabStop = true;
            this.radioSimple.Text = "Просто";
            this.radioSimple.UseVisualStyleBackColor = true;
            // 
            // radioHard
            // 
            this.radioHard.AutoSize = true;
            this.radioHard.Location = new System.Drawing.Point(15, 156);
            this.radioHard.Name = "radioHard";
            this.radioHard.Size = new System.Drawing.Size(67, 19);
            this.radioHard.TabIndex = 7;
            this.radioHard.TabStop = true;
            this.radioHard.Text = "Сложно";
            this.radioHard.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 393);
            this.Controls.Add(this.groupStudent);
            this.Controls.Add(this.buttonOK);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupStudent.ResumeLayout(false);
            this.groupStudent.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label textName;
        private System.Windows.Forms.Label textFamily;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label textFathName;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.GroupBox groupStudent;
        private System.Windows.Forms.ComboBox comboClass;
        private System.Windows.Forms.RadioButton radioHard;
        private System.Windows.Forms.RadioButton radioSimple;


    }
}

