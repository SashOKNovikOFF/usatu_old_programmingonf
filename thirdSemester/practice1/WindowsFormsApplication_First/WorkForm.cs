﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication_First
{
    public partial class WorkForm : Form
    {
        public WorkForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String stringStudent = String.Format("{0} {1} {2}", textBox2.Text, textBox1.Text, textBox3.Text);

            richTextBox1.Clear();
            richTextBox1.SelectionFont = new Font("Arial", 24, FontStyle.Regular);
            richTextBox1.AppendText("Ученик: ");
            
            richTextBox1.SelectionFont = new Font("Arial", 24, FontStyle.Bold);
            richTextBox1.AppendText(stringStudent);

            richTextBox1.SelectionFont = new Font("Arial", 24, FontStyle.Regular);
            richTextBox1.AppendText("\nКласс: ");

            richTextBox1.SelectionFont = new Font("Arial", 24, FontStyle.Bold);
            if (comboClass.SelectedIndex != -1)
                richTextBox1.AppendText(comboClass.Text);
            else
                MessageBox.Show("Не выбран класс обучающегося!");

            richTextBox1.SelectionFont = new Font("Arial", 24, FontStyle.Regular);
            if (radioSimple.Checked)
                richTextBox1.AppendText("\nВыбран лёгкий уровень сложности.");
            else if (radioHard.Checked)
                richTextBox1.AppendText("\nВыбран тяжёлый уровень сложности.");
            else
                MessageBox.Show("Не выбран уровень сложности!");

            Random myRandom = new Random();
            richTextBox1.AppendText("\nВаши задания: ");
            for (int i = 0; i < 10; i++ )
            {
                richTextBox1.SelectionFont = new Font("Arial", 24, FontStyle.Bold);
                richTextBox1.AppendText(String.Format("\n{0}. ", i+1));
                richTextBox1.SelectionFont = new Font("Arial", 24, FontStyle.Regular);
                richTextBox1.AppendText(String.Format("{0} x {1} = ?", myRandom.Next(30), myRandom.Next(30)));
            }

                richTextBox1.AppendText("\nВот и всё.");
        }

        private void comboClass_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
