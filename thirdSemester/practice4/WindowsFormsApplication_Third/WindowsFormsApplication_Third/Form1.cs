﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication_Third
{
    public partial class mainForm : Form
    {
        public mainForm()
        {
            InitializeComponent();
            comboBox.Items.Add(Grid.GridDisplayMode.Numbers);
            comboBox.Items.Add(Grid.GridDisplayMode.Colors);
            comboBox.Items.Add(Grid.GridDisplayMode.NumberAndColors);

            comboBox.SelectedItem = Grid.GridDisplayMode.NaN;
            grid.DisplayMode = Grid.GridDisplayMode.NaN;
            statusLabel.Text = "Вы ничего не выбрали!";
        }

        private void buttonRandom_Click(object sender, EventArgs e)
        {
            Random randomNumber = new Random();
            
            if (grid.DisplayMode != Grid.GridDisplayMode.NaN)
                grid[randomNumber.Next(grid.ColumnCount), randomNumber.Next(grid.RowCount)] = randomNumber.Next(grid.Colors.Length - 1);
            
            grid.Refresh();
        }
        private void grid_CellClick(object sender, int column, int row)
        {
            Random randomNumber = new Random();

            if (grid.DisplayMode != Grid.GridDisplayMode.NaN)
                grid[column, row] = randomNumber.Next(grid.Colors.Length - 1);

            grid.Refresh();
        }

        private void comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            grid.DisplayMode = (Grid.GridDisplayMode)comboBox.SelectedItem;
            statusLabel.Text = "";
            grid.Refresh();
        }
    }
}
