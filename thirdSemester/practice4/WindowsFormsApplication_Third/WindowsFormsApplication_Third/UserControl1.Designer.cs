﻿namespace WindowsFormsApplication_Third
{
    partial class buttonControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonPanel = new System.Windows.Forms.TableLayoutPanel();
            this.buttonNumbered = new System.Windows.Forms.Button();
            this.buttonColored = new System.Windows.Forms.Button();
            this.buttonClear = new WindowsFormsApplication_Third.RedButton();
            this.buttonPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonPanel
            // 
            this.buttonPanel.AutoSize = true;
            this.buttonPanel.ColumnCount = 1;
            this.buttonPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.buttonPanel.Controls.Add(this.buttonNumbered, 0, 0);
            this.buttonPanel.Controls.Add(this.buttonColored, 0, 1);
            this.buttonPanel.Controls.Add(this.buttonClear, 0, 2);
            this.buttonPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonPanel.Location = new System.Drawing.Point(0, 0);
            this.buttonPanel.Name = "buttonPanel";
            this.buttonPanel.RowCount = 3;
            this.buttonPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.buttonPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.buttonPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.buttonPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.buttonPanel.Size = new System.Drawing.Size(100, 170);
            this.buttonPanel.TabIndex = 1;
            // 
            // buttonNumbered
            // 
            this.buttonNumbered.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.buttonNumbered.AutoSize = true;
            this.buttonNumbered.Location = new System.Drawing.Point(25, 3);
            this.buttonNumbered.Name = "buttonNumbered";
            this.buttonNumbered.Size = new System.Drawing.Size(49, 50);
            this.buttonNumbered.TabIndex = 0;
            this.buttonNumbered.Text = "Числа";
            this.buttonNumbered.UseVisualStyleBackColor = true;
            this.buttonNumbered.Click += new System.EventHandler(this.buttonNumbered_Click);
            // 
            // buttonColored
            // 
            this.buttonColored.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.buttonColored.AutoSize = true;
            this.buttonColored.Location = new System.Drawing.Point(26, 59);
            this.buttonColored.Name = "buttonColored";
            this.buttonColored.Size = new System.Drawing.Size(48, 50);
            this.buttonColored.TabIndex = 1;
            this.buttonColored.Text = "Цвета";
            this.buttonColored.UseVisualStyleBackColor = true;
            this.buttonColored.Click += new System.EventHandler(this.buttonColored_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.buttonClear.AutoSize = true;
            this.buttonClear.BackColor = System.Drawing.Color.Red;
            this.buttonClear.Crossed = false;
            this.buttonClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonClear.ForeColor = System.Drawing.Color.White;
            this.buttonClear.Location = new System.Drawing.Point(22, 115);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(56, 52);
            this.buttonClear.TabIndex = 2;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = false;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buttonPanel);
            this.Name = "buttonControl";
            this.Size = new System.Drawing.Size(100, 170);
            this.buttonPanel.ResumeLayout(false);
            this.buttonPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel buttonPanel;
        private System.Windows.Forms.Button buttonNumbered;
        private System.Windows.Forms.Button buttonColored;
        private RedButton buttonClear;

    }
}
