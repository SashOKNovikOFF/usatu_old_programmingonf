﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication_Third
{
    public partial class RedButton : Button
    {
        public RedButton()
        {
            InitializeComponent();
        }

        private bool crossed;
        public bool Crossed
        {
            set
            {
                crossed = value;
                this.Refresh();
            }
            get
            {
                return crossed;
            }
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
            if (Crossed)
            {
                Pen whitePen = new Pen(Color.White, 3);
                Graphics place = pe.Graphics;
                place.DrawLine(whitePen, 0, 0, Size.Width, Size.Height);
                place.DrawLine(whitePen, Size.Width, 0, 0, Size.Height);
            }
        }
    }
}
