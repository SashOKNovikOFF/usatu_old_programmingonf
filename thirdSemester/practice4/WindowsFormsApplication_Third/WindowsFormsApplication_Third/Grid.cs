﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication_Third
{
    public partial class Grid : Control
    {
        public Grid()
        {
            InitializeComponent();

            Color[] colors = { Color.Azure, Color.Cyan, Color.Orange, Color.Pink, Color.Chocolate };
            Colors = colors;
        }

        int minCellSize = 5;
        int cellSize = 10;
        int rowCount = 10;
        int columnCount = 10;

        [DefaultValue(10)]
        public int CellSize
        {
            set
            {
                if (value < minCellSize)
                    cellSize = minCellSize;
                else
                    cellSize = value;
                this.Refresh();
            }
            get
            {
                return cellSize;
            }
        }
        [DefaultValue(10)]
        public int RowCount
        {
            set
            {
                rowCount = value;
                this.Refresh();
            }
            get
            {
                return rowCount;
            }
        }
        [DefaultValue(10)]
        public int ColumnCount
        {
            set
            {
                columnCount = value;
                this.Refresh();
            }
            get
            {
                return columnCount;
            }
        }
               
        static Color[] colors;
        [DefaultValue(null)]
        public Color[] Colors { get; set; }

        Pen gridPen = new Pen(Color.Black, 1);
        [DefaultValue(null)]
        public Color LineColor
        {
            set
            {
                gridPen.Color = value;
                this.Refresh();
            }
            get
            {
                return gridPen.Color;
            }
        }
        SolidBrush gridBrush = new SolidBrush(Color.White);
        [DefaultValue(null)]
        public Color BrushColor
        {
            set
            {
                gridBrush.Color = value;
                this.Refresh();
            }
            get
            {
                return gridBrush.Color;
            }
        }

        const int cellsMaxWidth = 100;
        const int cellsMaxHeight = 100;
        int[,] data = new int[cellsMaxWidth, cellsMaxHeight];
        public void SetData(int i, int j, int value)
        {
            data[i, j] = value;
        }
        public int GetData(int i, int j)
        {
            return data[i, j];
        }
        public int this[int i, int j]
        {
            set
            {
                data[i, j] = value;
            }
            get
            {
                return data[i, j];
            }
        }

        public delegate void CellHandler(Object sender, int column, int row);
        public event CellHandler CellClick;
        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);

            if (ContainsPoint(e.Location))
                if (CellClick != null)
                    CellClick(this, e.X / cellSize, e.Y / cellSize);
        }

        public enum GridDisplayMode { Numbers, Colors, NumberAndColors, NaN };
        public GridDisplayMode DisplayMode { set; get; }

        public bool ContainsPoint(Point point)
        {
            if ((point.X >= 0) && (point.X <= CellSize*columnCount) && (point.Y >= 0) && (point.Y <= CellSize*rowCount))
                return true;
            else
                return false;
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);

            Graphics place = pe.Graphics;
            int widthGrid = ColumnCount * CellSize;
            int heightGrid = RowCount * CellSize;
           
            // уменьшение сетки, при слишком больших значениях
            if (widthGrid > this.Size.Width)
            {
                widthGrid = (this.Size.Width / CellSize) * CellSize;
                ColumnCount = widthGrid / CellSize;
            }
            if (heightGrid > this.Size.Height)
            {
                heightGrid = (this.Size.Height / CellSize) * CellSize;
                RowCount = heightGrid / CellSize;
            }

            // Построение сетки
            for (int i = 0; i <= widthGrid; i += CellSize)
                place.DrawLine(gridPen, i, 0, i, heightGrid);
            for (int i = 0; i <= heightGrid; i += CellSize)
                place.DrawLine(gridPen, 0, i, widthGrid, i);

            // Данные по отрисовке чисел массива data в сетке
            int relativeSize = 3;
            SolidBrush brushBlack = new SolidBrush(Color.Black);
            Font fontNumber = new Font("Times New Roman", CellSize / relativeSize);

            for (int i = 0; i < ColumnCount; i++)
                for (int j = 0, x, y; j < RowCount; j++)
                    if (data[i, j] != 0)
                    {
                        SolidBrush cellBrush = new SolidBrush(ForeColor);
                        x = i * CellSize + CellSize / relativeSize;
                        y = j * CellSize + CellSize / relativeSize;
                        if (data[i, j] < Colors.Length)
                            cellBrush.Color = Colors[data[i, j]];
                        else
                            cellBrush.Color = mainForm.DefaultBackColor;
                        
                        switch(DisplayMode)
                        {
                            case GridDisplayMode.Colors:
                                    place.FillRectangle(cellBrush, i * CellSize + 1, j * CellSize + 1, CellSize - 1, CellSize - 1);
                                break;
                            case GridDisplayMode.Numbers:
                                    place.DrawString(data[i, j].ToString(), fontNumber, brushBlack, x, y);
                                break;
                            case GridDisplayMode.NumberAndColors:
                                {
                                    place.FillRectangle(cellBrush, i * CellSize + 1, j * CellSize + 1, CellSize - 1, CellSize - 1);
                                    place.DrawString(data[i, j].ToString(), fontNumber, brushBlack, x, y);
                                }
                                break;
                        }

                        
                        
                    }
        }
    }
}
