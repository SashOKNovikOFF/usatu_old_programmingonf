﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PatternsTemplate
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            comboBoxFlightClass.DataSource = Enum.GetValues(typeof(FlightClass));
            comboBoxFlightTime.DataSource = Enum.GetValues(typeof(FlightTime));
            comboBoxFlightDestination.DataSource = new BindingSource(FlightDestination.Instance.DistanceToDestination.Keys, null);

            comboBoxFlightDestination.SelectedItem = null;
            comboBoxFlightClass.SelectedItem = null;
            comboBoxFlightTime.SelectedItem = null;
        }

        private void buttonCalc_Click(object sender, EventArgs e)
        {

        }

        private void buttonUndo_Click(object sender, EventArgs e)
        {

        }
    }
}
