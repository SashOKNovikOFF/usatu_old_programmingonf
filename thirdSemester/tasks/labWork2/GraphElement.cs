﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;

namespace GraphObjects
{
    class GraphElement
    {
        Color c;
        int w, h;
        int x, y;

        Brush brush;
        static Random r = new Random();
        public int X
        {
            get { return x; }
            set { x = value; }
        }
        public Point Location { 
            get { return new Point(x, y); }
            set { x = value.X; y = value.Y; }
        }

        public GraphElement( ) {
            Color[] cols = { Color.Red, Color.Green, Color.Yellow, Color.Tomato, Color.Cyan };
            c = cols[r.Next(cols.Length)];
            x = r.Next(200);
            y = r.Next(200);
            w = 50;
            h = 50;
            brush = new SolidBrush(c);
        }

        public void draw(Graphics g)
        {
            g.FillRectangle(brush, x, y, w, h);
            g.DrawRectangle(Pens.Black, x, y, w, h);
        }
    }
}
