﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;

namespace WindowsFormsApplication_Second
{
    abstract class GraphObject
    {
        Color myColor;
        public Brush brush;
        public static Pen selectedColor = new Pen(Color.Orange, 5);
        static Color[] colors = { Color.Red, Color.Green, Color.YellowGreen, Color.Tomato, Color.Cyan };
        static Random randomNumber = new Random();
        static int randMax = 300;
        int x, y;
        static int minX = 30;
        static int minY = 30;
        static int maxX = 300;
        static int maxY = 300;

        public int X
        {
            get { return x; }
            set
            {
                if (value < minX) { throw new ArgumentException("x < " + minX + "!"); };
                if (value > maxX) { throw new ArgumentException("x > " + maxX + "!"); };
                x = value;
            }
        }
        public int Y
        {
            get { return y; }
            set
            {
                if (value < minY) { throw new ArgumentException("y < " + minY + "!"); };
                if (value > maxY) { throw new ArgumentException("y > " + maxY + "!"); };
                y = value;
            }
        }
        public bool Selected { get; set; }

        public abstract bool ContainsPoint(Point point);
        public abstract void Draw(Graphics place);

        public GraphObject()
        {
            myColor = colors[randomNumber.Next(colors.Length)];
            X = minX + (maxX - minX) % (1 + randomNumber.Next(randMax - 1));
            Y = minY + (maxY - minY) % (1 + randomNumber.Next(randMax - 1));
            brush = new SolidBrush(myColor);
        }

        public GraphObject(int x, int y)
        {
            myColor = colors[randomNumber.Next(colors.Length)];
            X = x;
            Y = y;
            brush = new SolidBrush(myColor);
        }

        public void MoveGraphObject(int dx, int dy)
        {
            X = X + dx;
            Y = Y + dy;
        }
    }

    class Ellipse : GraphObject
    {
        static int width = 70;
        static int height = 40;

        public Ellipse() : base()
        {
        }

        public Ellipse(int x, int y) : base(x, y)
        {
        }

        public override bool ContainsPoint(Point point)
        {
            if ((Math.Pow((X - point.X), 2) / Math.Pow(width / 2, 2) + Math.Pow((Y - point.Y), 2) / Math.Pow(height / 2, 2)) <= 1)
                return true;
            else
                return false;
        }

        public override void Draw(Graphics place)
        {
            Pen tempPen;

            place.FillEllipse(brush, X - width / 2, Y - height / 2, width, height);
            if (Selected)
                tempPen = selectedColor;
            else
                tempPen = Pens.Black;
            place.DrawEllipse(tempPen, X - width / 2, Y - height / 2, width, height);
        }
    }

    class Rectangle : GraphObject
    {
        static int width = 50;
        static int height = 50;
        
        public Rectangle() : base()
        {
        }

        public Rectangle(int x, int y) : base(x, y)
        {
        }

        public override bool ContainsPoint(Point point)
        {
            if ((point.X >= (X - width / 2)) && (point.X <= (X + width / 2)) && (point.Y >= (Y - height / 2)) && (point.Y <= (Y + height / 2)))
                return true;
            else
                return false;
        }

        public override void Draw(Graphics place)
        {
            Pen tempPen;

            place.FillRectangle(brush, X - width / 2, Y - height / 2, width, height);
            if (Selected)
                tempPen = selectedColor;
            else
                tempPen = Pens.Black;
            place.DrawRectangle(tempPen, X - width / 2, Y - height / 2, width, height);
        }
    }
}
