﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication_Second
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        List<GraphObject> elements = new List<GraphObject>();
        GraphObject lastSelected;
        static Random randomNumber = new Random();

        private void MyPanel_Paint(object sender, PaintEventArgs e)
        {
            foreach (GraphObject element in elements)
            {
                element.Draw(e.Graphics);
            }
        }

        private void AddObjectButton_Click(object sender, EventArgs e)
        {
            if (randomNumber.Next(-1, 2) == 0)
                elements.Add(new Rectangle());
            else
                elements.Add(new Ellipse());

            MyPanel.Refresh();
        }

        private void DeleteObjectButton_Click(object sender, EventArgs e)
        {
            elements.Remove(lastSelected);
            MyPanel.Refresh();
        }

        private void MyPanel_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                if (randomNumber.Next(-1, 2) == 0)
                    elements.Add(new Rectangle(e.X, e.Y));
                else
                    elements.Add(new Ellipse(e.X, e.Y));
                StatusLabel.Text = "Строка состояния";
                MyPanel.Refresh();
            }
            catch(Exception ex)
            {
                StatusLabel.Text = ex.Message;
            }
        }

        int x0, y0;
        int dx, dy;
        bool dragging = false;

        private void MyPanel_MouseDown(object sender, MouseEventArgs e)
        {
            x0 = dx = e.X;
            y0 = dy = e.Y;
            dragging = true;

            foreach (GraphObject element in elements)
            {
                if (element.ContainsPoint(e.Location))
                {
                    element.Selected = true;
                    if (lastSelected != null)
                        lastSelected.Selected = false;
                    lastSelected = element;
                    
                    MyPanel.Refresh();
                }
            }
        }

        private void MyPanel_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (dragging)
                {
                    lastSelected.MoveGraphObject(e.X - dx, e.Y - dy);
                    StatusLabel.Text = "Строка состояния";
                    MyPanel.Refresh();
                    
                    dx = e.X;
                    dy = e.Y;
                }
            }
            catch (Exception ex)
            {
                StatusLabel.Text = ex.Message;
            }
        }

        private void MyPanel_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
            if (!((x0 == dx) && (y0 == dy)))
                if (lastSelected != null)
                {
                    lastSelected.Selected = false;
                    lastSelected = null;
                };
            MyPanel.Refresh();
        }

        private void CreateButton_Click(object sender, EventArgs e)
        {
            try
            {
                lastSelected.MoveGraphObject(10, 10);
                StatusLabel.Text = "Строка состояния";
                MyPanel.Refresh();
            }
            catch (Exception ex)
            {
                StatusLabel.Text = ex.Message;
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            try
            {
                lastSelected.MoveGraphObject(-10, -10);
                StatusLabel.Text = "Строка состояния";
                MyPanel.Refresh();
            }
            catch (Exception ex)
            {
                StatusLabel.Text = ex.Message;
            }
        }

        private void DeleteMenuItem_Click(object sender, EventArgs e)
        {
            elements.Clear();
            MyPanel.Refresh();
        }
    }
}
