﻿namespace WindowsFormsApplication_Second
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.objectsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AddMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DeleteMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MyToolStrip = new System.Windows.Forms.ToolStrip();
            this.CreateButton = new System.Windows.Forms.ToolStripButton();
            this.CancelButton = new System.Windows.Forms.ToolStripButton();
            this.Separator_1 = new System.Windows.Forms.ToolStripSeparator();
            this.AddObjectButton = new System.Windows.Forms.ToolStripButton();
            this.DeleteObjectButton = new System.Windows.Forms.ToolStripButton();
            this.Separator_2 = new System.Windows.Forms.ToolStripSeparator();
            this.ExitButton = new System.Windows.Forms.ToolStripButton();
            this.MyStatusPanel = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.MyPanel = new System.Windows.Forms.Panel();
            this.MainMenu.SuspendLayout();
            this.MyToolStrip.SuspendLayout();
            this.MyStatusPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainMenu
            // 
            this.MainMenu.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.objectsToolStripMenuItem});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(535, 24);
            this.MainMenu.TabIndex = 0;
            this.MainMenu.Text = "MainMenu";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ExitMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // ExitMenuItem
            // 
            this.ExitMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ExitMenuItem.Image")));
            this.ExitMenuItem.Name = "ExitMenuItem";
            this.ExitMenuItem.Size = new System.Drawing.Size(118, 22);
            this.ExitMenuItem.Text = "Выход";
            this.ExitMenuItem.Click += new System.EventHandler(this.Exit_Click);
            // 
            // objectsToolStripMenuItem
            // 
            this.objectsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddMenuItem,
            this.DeleteMenuItem});
            this.objectsToolStripMenuItem.Name = "objectsToolStripMenuItem";
            this.objectsToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.objectsToolStripMenuItem.Text = "Objects";
            // 
            // AddMenuItem
            // 
            this.AddMenuItem.Name = "AddMenuItem";
            this.AddMenuItem.Size = new System.Drawing.Size(197, 22);
            this.AddMenuItem.Text = "Добавить объект";
            this.AddMenuItem.Click += new System.EventHandler(this.AddObjectButton_Click);
            // 
            // DeleteMenuItem
            // 
            this.DeleteMenuItem.Name = "DeleteMenuItem";
            this.DeleteMenuItem.Size = new System.Drawing.Size(197, 22);
            this.DeleteMenuItem.Text = "Удалить все объекты";
            this.DeleteMenuItem.Click += new System.EventHandler(this.DeleteMenuItem_Click);
            // 
            // MyToolStrip
            // 
            this.MyToolStrip.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.MyToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CreateButton,
            this.CancelButton,
            this.Separator_1,
            this.AddObjectButton,
            this.DeleteObjectButton,
            this.Separator_2,
            this.ExitButton});
            this.MyToolStrip.Location = new System.Drawing.Point(0, 24);
            this.MyToolStrip.Name = "MyToolStrip";
            this.MyToolStrip.Size = new System.Drawing.Size(535, 47);
            this.MyToolStrip.TabIndex = 1;
            this.MyToolStrip.Text = "MyToolStrip";
            // 
            // CreateButton
            // 
            this.CreateButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.CreateButton.Image = ((System.Drawing.Image)(resources.GetObject("CreateButton.Image")));
            this.CreateButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CreateButton.Name = "CreateButton";
            this.CreateButton.Size = new System.Drawing.Size(36, 44);
            this.CreateButton.Text = "Передвинуть объект";
            this.CreateButton.Click += new System.EventHandler(this.CreateButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.CancelButton.Image = ((System.Drawing.Image)(resources.GetObject("CancelButton.Image")));
            this.CancelButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(36, 44);
            this.CancelButton.Text = "Отменить движение";
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // Separator_1
            // 
            this.Separator_1.Name = "Separator_1";
            this.Separator_1.Size = new System.Drawing.Size(6, 47);
            // 
            // AddObjectButton
            // 
            this.AddObjectButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.AddObjectButton.Image = ((System.Drawing.Image)(resources.GetObject("AddObjectButton.Image")));
            this.AddObjectButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AddObjectButton.Name = "AddObjectButton";
            this.AddObjectButton.Size = new System.Drawing.Size(36, 44);
            this.AddObjectButton.Text = "Добавить объект";
            this.AddObjectButton.Click += new System.EventHandler(this.AddObjectButton_Click);
            // 
            // DeleteObjectButton
            // 
            this.DeleteObjectButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.DeleteObjectButton.Image = ((System.Drawing.Image)(resources.GetObject("DeleteObjectButton.Image")));
            this.DeleteObjectButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.DeleteObjectButton.Name = "DeleteObjectButton";
            this.DeleteObjectButton.Size = new System.Drawing.Size(36, 44);
            this.DeleteObjectButton.Text = "Удалить объект";
            this.DeleteObjectButton.Click += new System.EventHandler(this.DeleteObjectButton_Click);
            // 
            // Separator_2
            // 
            this.Separator_2.Name = "Separator_2";
            this.Separator_2.Size = new System.Drawing.Size(6, 47);
            // 
            // ExitButton
            // 
            this.ExitButton.Image = ((System.Drawing.Image)(resources.GetObject("ExitButton.Image")));
            this.ExitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(49, 44);
            this.ExitButton.Text = "Выход";
            this.ExitButton.TextDirection = System.Windows.Forms.ToolStripTextDirection.Vertical90;
            this.ExitButton.Click += new System.EventHandler(this.Exit_Click);
            // 
            // MyStatusPanel
            // 
            this.MyStatusPanel.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel});
            this.MyStatusPanel.Location = new System.Drawing.Point(0, 421);
            this.MyStatusPanel.Name = "MyStatusPanel";
            this.MyStatusPanel.Size = new System.Drawing.Size(535, 22);
            this.MyStatusPanel.TabIndex = 2;
            this.MyStatusPanel.Text = "MyStatusPanel";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(99, 17);
            this.StatusLabel.Text = "Строка состояния";
            // 
            // MyPanel
            // 
            this.MyPanel.Location = new System.Drawing.Point(12, 75);
            this.MyPanel.Name = "MyPanel";
            this.MyPanel.Size = new System.Drawing.Size(511, 343);
            this.MyPanel.TabIndex = 3;
            this.MyPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.MyPanel_Paint);
            this.MyPanel.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.MyPanel_MouseDoubleClick);
            this.MyPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MyPanel_MouseDown);
            this.MyPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MyPanel_MouseMove);
            this.MyPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MyPanel_MouseUp);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(535, 443);
            this.Controls.Add(this.MyPanel);
            this.Controls.Add(this.MyStatusPanel);
            this.Controls.Add(this.MyToolStrip);
            this.Controls.Add(this.MainMenu);
            this.MainMenuStrip = this.MainMenu;
            this.Name = "MainForm";
            this.Text = "Лабораторная работа №2";
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.MyToolStrip.ResumeLayout(false);
            this.MyToolStrip.PerformLayout();
            this.MyStatusPanel.ResumeLayout(false);
            this.MyStatusPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MainMenu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStrip MyToolStrip;
        private System.Windows.Forms.ToolStripButton CreateButton;
        private System.Windows.Forms.ToolStripButton CancelButton;
        private System.Windows.Forms.StatusStrip MyStatusPanel;
        private System.Windows.Forms.ToolStripMenuItem objectsToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton ExitButton;
        private System.Windows.Forms.ToolStripMenuItem ExitMenuItem;
        private System.Windows.Forms.ToolStripSeparator Separator_1;
        private System.Windows.Forms.ToolStripMenuItem AddMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DeleteMenuItem;
        private System.Windows.Forms.ToolStripButton AddObjectButton;
        private System.Windows.Forms.ToolStripButton DeleteObjectButton;
        private System.Windows.Forms.ToolStripSeparator Separator_2;
        private System.Windows.Forms.Panel MyPanel;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel;
    }
}

